# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi_versionizer import versionize
from rich.console import Console
from starlette.responses import RedirectResponse

from tedective_api.routers import award, contract, organization, release, tender
from tedective_api.routers.graph import award as award_graph
from tedective_api.routers.graph import award_supplier
from tedective_api.routers.graph import contract as contract_graph
from tedective_api.routers.graph import release as release_graph
from tedective_api.routers.graph import tender as tender_graph

console = Console()

# Define FastAPI app
app = FastAPI(
    title="TEDective API",
    # swagger_ui_parameters={"defaultModelsExpandDepth": -1}
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
    allow_credentials=True,
)


# adjust so it is checked if the database already exists, instead of crashing
# @app.on_event("startup")
# async def on_startup():
#     init_db()


app.include_router(release.router)
app.include_router(award_graph.router)
app.include_router(contract_graph.router)
app.include_router(tender_graph.router)
app.include_router(organization.router)
app.include_router(award_supplier.router)
app.include_router(release_graph.router)
# app.include_router(implementation.router)
# app.include_router(planning.router)
app.include_router(tender.router)
app.include_router(contract.router)
app.include_router(award.router)

versions = versionize(
    app=app,
    prefix_format="/v{major}",
    docs_url="/docs",
    redoc_url="/redoc",
    enable_latest=True,
)


# Redirect root to /docs
@app.get("/", response_class=RedirectResponse, include_in_schema=True)
async def redirect_fastapi():
    return RedirectResponse("/latest/docs", status_code=303)


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
