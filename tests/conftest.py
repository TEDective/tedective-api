# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import asyncio
from typing import Generator

import pytest
import pytest_asyncio

from tedective_parser.database import cleanup_testing_db, prepare_testing_db


@pytest_asyncio.fixture(scope="session")
def setUp():
    client = prepare_testing_db()
    yield client
    cleanup_testing_db()
