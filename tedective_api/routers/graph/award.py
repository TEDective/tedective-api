# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import uuid

from fastapi import APIRouter, Depends, HTTPException

from tedective_api.routers.award import read_award

router = APIRouter()


@router.get("/graph/award/{id}", tags=["awards"])
async def get_award(id: uuid.UUID):
    response = await read_award(id)
    graph = convert_response_to_graph(response)
    return graph


def convert_response_to_graph(award_data):
    nodes = []
    links = []

    award_node = {
        "name": award_data["title"],
        "id": award_data["id"],
        "description": award_data["description"],
        "status": award_data["status"],
        "date": award_data["date"],
        "value": award_data["value"],
        "items": award_data["items"],
        "contractPeriod": award_data["contractPeriod"],
        "documents": award_data["documents"],
        "amendments": award_data["amendments"],
        "amendment": award_data["amendment"],
    }
    nodes.append(award_node)

    for org in award_data["suppliers"]:
        suppliers_link = {
            "id": uuid.uuid4(),
            "source": org["id"],
            "target": award_data["id"],
        }
        suppliers_node = {
            "name": org["name"],
            "id": org["id"],
            "identifier": org["identifier"],
            "additionalIdentifiers": org["additionalIdentifiers"],
            "address": org["address"],
            "contactPoint": org["contactPoint"],
            "roles": org["roles"],
            "details": org["details"],
        }
        nodes.append(suppliers_node)
        links.append(suppliers_link)

    return {"nodes": nodes, "links": links}
