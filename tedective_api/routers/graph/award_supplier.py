# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import uuid

from fastapi import APIRouter, Depends, HTTPException

from tedective_api.routers.award import read_award_per_org

router = APIRouter()


@router.get("/graph/awards/supplier/{id}", tags=["awards"])
async def get_award(id: uuid.UUID):
    response = await read_award_per_org(id)
    graph = convert_response_to_graph(response)
    return graph


def convert_response_to_graph(award_data):
    nodes = []
    links = []

    for award in award_data:
        award_node = {
            "label": award["title"],
            "name": award["title"],
            "id": award["id"],
            "description": award["description"],
            "status": award["status"],
            "date": award["date"],
            "value": award["value"],
            "items": award["items"],
            "contractPeriod": award["contractPeriod"],
            "documents": award["documents"],
            "amendments": award["amendments"],
            "amendment": award["amendment"],
        }

        org = award["suppliers"][0]
        if org["id"] not in [node.get("id") for node in nodes]:
            supplier_node = {
                "label": org["name"],
                "name": org["name"],
                "id": org["id"],
                "identifier": org["identifier"],
                "additionalIdentifiers": org["additionalIdentifiers"],
                "address": org["address"],
                "contactPoint": org["contactPoint"],
                "roles": org["roles"],
                "details": org["details"],
            }

            nodes.append(supplier_node)

        supplier_link = {
            "id": uuid.uuid4(),
            "source": org["id"],
            "target": award["id"],
        }
        links.append(supplier_link)
        nodes.append(award_node)

    return {"nodes": nodes, "links": links}
