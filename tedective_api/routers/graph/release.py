# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import json
import uuid

from fastapi import APIRouter

from tedective_api.routers.award import read_award
from tedective_api.routers.release import (
    read_releases_per_buyer,
    read_releases_per_supplier,
)

router = APIRouter()


@router.get("/graph/releases/buyer/{id}", tags=["releases"])
async def get_release_buyer(id: uuid.UUID):
    response = await read_releases_per_buyer(id)
    graph = convert_response_to_graph(response)
    return graph


@router.get("/graph/releases/supplier/{id}", tags=["releases"])
async def get_release_supplier(id: uuid.UUID):
    response = await read_releases_per_supplier(id)
    graph = convert_response_to_graph(response)

    return graph


# Converting JSON response to graph endpoint
def convert_response_to_graph(release_data):
    nodes = []
    links = []

    unique_ids = set()

    for release in release_data:
        release_node = {
            "label": release["ocid"],
            "id": release["ocid"],
            "ocid": release["ocid"],
            "date": release["date"],
            "tag": release["tag"],
            "initiationType": release["initiationType"],
            "planning": release["planning"],
            "language": release["language"],
            "relatedProcesses": release["relatedProcesses"],
        }

        if release_node["id"] not in unique_ids:
            nodes.append(release_node)
            unique_ids.add(release_node["id"])

        buyer = release["buyer"][0]
        buyer_node = {
            "label": buyer["name"],
            "id": buyer["id"],
            "name": buyer["name"],
            "identifier": buyer["identifier"],
            "additionalIdentifiers": buyer["additionalIdentifiers"],
            "address": buyer["address"],
            "contactPoint": buyer["contactPoint"],
            "roles": buyer["roles"],
            "details": buyer["details"],
        }
        buyer_link = {
            "id": uuid.uuid4(),
            "source": release["ocid"],
            "target": buyer["id"],
        }

        if buyer_node["id"] not in unique_ids:
            nodes.append(buyer_node)
            unique_ids.add(buyer_node["id"])
        
        links.append(buyer_link) 

        for party in release["parties"]:
            if party["id"] not in [node.get("id") for node in nodes]:
                party_node = {
                    "label": party["name"],
                    "id": party["id"],
                    "name": party["name"],
                    "identifier": party["identifier"],
                    "additionalIdentifiers": party["additionalIdentifiers"],
                    "address": party["address"],
                    "contactPoint": party["contactPoint"],
                    "roles": party["roles"],
                    "details": party["details"],
                }
                party_link = {
                    "id": uuid.uuid4(),
                    "source": release["ocid"],
                    "target": party["id"],
                }
                nodes.append(party_node)
                links.append(party_link)

        tender = release["tender"]
        tid = uuid.uuid4()
        tender_node = {
            "label": tender["title"],
            "id": tid,
            "ocid": tender["id"],
            "title": tender["title"],
            "description": tender["description"],
            "status": tender["status"],
            "items": tender["items"],
            "value": tender["value"],
            "minValue": tender["minValue"],
            "procurementMethod": tender["procurementMethod"],
            "procurementMethodDetails": tender["procurementMethodDetails"],
            "procurementMethodRationale": tender["procurementMethodRationale"],
            "additionalProcurementCategories": tender[
                "additionalProcurementCategories"
            ],
            "awardCriteria": tender["awardCriteria"],
            "awardCriteriaDetails": tender["awardCriteriaDetails"],
            "submissionMethod": tender["submissionMethod"],
            "submissionMethodDetails": tender["submissionMethodDetails"],
            "tenderPeriod": tender["tenderPeriod"],
            "enquiryPeriod": tender["enquiryPeriod"],
            "hasEnquiries": tender["hasEnquiries"],
            "eligibilityCriteria": tender["eligibilityCriteria"],
            "awardPeriod": tender["awardPeriod"],
            "contractPeriod": tender["contractPeriod"],
            "numberOfTenderers": tender["numberOfTenderers"],
            "documents": tender["documents"],
            "amendments": tender["amendment"],
            "amendment": tender["amendment"],
            "crossBorderLaw": tender["crossBorderLaw"],
        }
        tender_link = {
            "id": uuid.uuid4(),
            "source": release["ocid"],
            "target": tid,
        }
        nodes.append(tender_node)
        links.append(tender_link)

        if tender["procuringEntity"] is not None:
            for org in tender["procuringEntity"]:
                if org["id"] not in [node.get("id") for node in nodes]:
                    procuring_link = {
                        "id": uuid.uuid4(),
                        "source": tid,
                        "target": org["id"],
                    }
                    procuring_node = {
                        "label": org["name"],
                        "id": org["id"],
                        "name": org["name"],
                        "identifier": org["identifier"],
                        "additionalIdentifiers": org["additionalIdentifiers"],
                        "address": org["address"],
                        "contactPoint": org["contactPoint"],
                        "roles": org["roles"],
                        "details": org["details"],
                    }
                    nodes.append(procuring_node)
                    links.append(procuring_link)

        for org in tender["tenderers"]:
            if org["id"] not in [node.get("id") for node in nodes]:
                tenderer_link = {
                    "id": uuid.uuid4(),
                    "source": tid,
                    "target": org["id"],
                }
                tenderer_node = {
                    "label": org["name"],
                    "id": org["id"],
                    "name": org["name"],
                    "identifier": org["identifier"],
                    "additionalIdentifiers": org["additionalIdentifiers"],
                    "address": org["address"],
                    "contactPoint": org["contactPoint"],
                    "roles": org["roles"],
                    "details": org["details"],
                }
                nodes.append(tenderer_node)
                links.append(tenderer_link)

        for award_data in release["awards"]:
            award_node = {
                "label": award_data["title"],
                "id": award_data["id"],
                "title": award_data["title"],
                "description": award_data["description"],
                "status": award_data["status"],
                "date": award_data["date"],
                "value": award_data["value"],
                "items": award_data["items"],
                "contractPeriod": award_data["contractPeriod"],
                "documents": award_data["documents"],
                "amendments": award_data["amendments"],
                "amendment": award_data["amendment"],
            }

            award_link = {
                "id": uuid.uuid4(),
                "source": release["ocid"],
                "target": award_data["id"],
            }
            nodes.append(award_node)
            links.append(award_link)

            for org in award_data["suppliers"]:
                if org["id"] not in [node.get("id") for node in nodes]:
                    suppliers_link = {
                        "id": uuid.uuid4(),
                        "source": org["id"],
                        "target": award_data["id"],
                    }
                    suppliers_node = {
                        "label": org["name"],
                        "name": org["name"],
                        "id": org["id"],
                        "identifier": org["identifier"],
                        "additionalIdentifiers": org["additionalIdentifiers"],
                        "address": org["address"],
                        "contactPoint": org["contactPoint"],
                        "roles": org["roles"],
                        "details": org["details"],
                    }
                    nodes.append(suppliers_node)
                    links.append(suppliers_link)

        for contract_data in release["contracts"]:
            contract_node = {
                "label": contract_data["title"],
                "id": contract_data["id"],
                "title": contract_data["title"],
                "awardID": contract_data["awardID"],
                "description": contract_data["description"],
                "period": contract_data["period"],
                "value": contract_data["value"],
                "dateSigned": contract_data["dateSigned"],
                "documents": contract_data["documents"],
                "implementation": contract_data["implementation"],
                "items": contract_data["items"],
                "relatedProcesses": contract_data["relatedProcesses"],
                "milestones": contract_data["milestones"],
                "amendments": contract_data["amendments"],
                "amendment": contract_data["amendment"],
            }

            contract_link = {
                "id": uuid.uuid4(),
                "source": release["ocid"],
                "target": contract_data["id"],
            }

            nodes.append(contract_node)
            links.append(contract_link)

            # award = await read_award(contract_data["awardID"])
            # print(award)

            # if ()
            # award = await read_award(contract_data["awardID"])
            #
            # award_node = {
            #     "id": award["id"],
            #     "name": award["title"],
            #     "description": award["description"],
            #     "status": award["status"],
            #     "date": award["date"],
            #     "value": award["value"],
            #     "items": award["items"],
            #     "contractPeriod": award["contractPeriod"],
            #     "documents": award["documents"],
            #     "amendments": award["amendments"],
            #     "amendment": award["amendment"],
            # }
            #
            # award_link = {
            #     "id": uuid.uuid4(),
            #     "source": contract_data["id"],
            #     "target": award["id"],
            # }
            # nodes.append(award_node)
            # links.append(award_link)
            #
            # for org in award["suppliers"]:
            #     if org["id"] not in [node.get("id") for node in nodes]:
            #         suppliers_link = {
            #             "id": uuid.uuid4(),
            #             "source": org["id"],
            #             "target": award["id"],
            #         }
            #         suppliers_node = {
            #             "name": org["name"],
            #             "id": org["id"],
            #             "identifier": org["identifier"],
            #             "additionalIdentifiers": org["additionalIdentifiers"],
            #             "address": org["address"],
            #             "contactPoint": org["contactPoint"],
            #             "roles": org["roles"],
            #             "details": org["details"],
            #         }
            #         nodes.append(suppliers_node)
            #         links.append(suppliers_link)

    return {"nodes": nodes, "links": links}
