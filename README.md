<!--
SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# TEDective API
[![Black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

The TEDective API makes use of the OCDS data stored in [TerminusDB](https://terminusdb.com), and allows to make requests for certain entities such as organizations, awards, contracts or releases. It can give responses in JSON as well as in [react-force-graph](https://github.com/vasturiano/react-force-graph#input-json-syntax)-ready format.
Part of [TEDective Project](https://tedective.org).

## Table of Contents

- [TEDective API](#tedective-api)
  - [Table of Contents](#table-of-contents)
  - [Background](#background)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Background

The TEDective project aims to make European public procurement data from
Tenders Electronic Daily (TED) explorable for non-experts. This repository
contains all Python code used to build the API that is on top of TerminusDB
with (already converted to OCDS) TED data.

Part of [TEDective Project](https://tedective.org)

## Install
[GNU Make](https://www.gnu.org/software/make/), [Docker](https://docker.com/) and [docker compose](https://docs.docker.com/compose/) are required.

For detailed instructions please refer to the [TEDective documentation](https://docs.tedective.org/api/quickstart.mdx).

## Usage

:construction: TODO: (probably put the reference to documentation)

## Maintainers
[@micgor32](https://github.com/micgor32)

## Contributing
To get setup, you need [poetry](https://python-poetry.org/). Then you need to run:

```bash
poetry install
poetry shell
```

When running on a local host, you would most likely want edit use [TEDective Parser](https://git.fsfe.org/TEDective/parser/), to setup up the TerminusDB instance with ingested data. By default API will use [.env](https://git.fsfe.org/TEDective/api/src/branch/main/.env) file to determine TerminusDB address and login credentials.

More details on how to provide custom address and credentials can be found in the [TEDective documentation](https://docs.tedective.org/api/quickstart.mdx).

PRs are accepted.

Small note: If editing the README, please conform to the
[standard-readme](https://github.com/RichardLitt/standard-readme)
specification.

## License

AGPL-3.0-or-later © 2023 Free Software Foundation Europe e.V.