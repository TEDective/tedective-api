# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import uuid

from fastapi import APIRouter, Depends, HTTPException
from gql import Client, gql

from tedective_api.routers.tender import read_tender

router = APIRouter()


@router.get("/graph/tender/{id}", tags=["tenders"])
async def get_tender(id: str):
    response = await read_tender(id)
    graph = convert_response_to_graph(response)
    return graph


def convert_response_to_graph(tender_data):
    nodes = []
    links = []

    tender_node = {
        "title": tender_data["title"],
        "id": tender_data["id"],
        "description": tender_data["description"],
        "status": tender_data["status"],
        "items": tender_data["items"],
        "value": tender_data["value"],
        "minValue": tender_data["minValue"],
        "procurementMethod": tender_data["procurementMethod"],
        "procurementMethodDetails": tender_data["procurementMethodDetails"],
        "procurementMethodRationale": tender_data["procurementMethodRationale"],
        "mainProcurementCategory": tender_data["mainProcurementCategory"],
        "additionalProcurementCategories": tender_data[
            "additionalProcurementCategories"
        ],
        "awardCriteria": tender_data["awardCriteria"],
        "awardCriteriaDetails": tender_data["awardCriteriaDetails"],
        "submissionMethod": tender_data["submissionMethod"],
        "submissionMethodDetails": tender_data["submissionMethodDetails"],
        "tenderPeriod": tender_data["tenderPeriod"],
        "enquiryPeriod": tender_data["enquiryPeriod"],
        "hasEnquiries": tender_data["hasEnquiries"],
        "eligibilityCriteria": tender_data["eligibilityCriteria"],
        "awardPeriod": tender_data["awardPeriod"],
        "contractPeriod": tender_data["contractPeriod"],
        "numberOfTenderers": tender_data["numberOfTenderers"],
        "documents": tender_data["documents"],
        "amendments": tender_data["amendment"],
        "amendment": tender_data["amendment"],
        "crossBorderLaw": tender_data["crossBorderLaw"],
    }

    nodes.append(tender_node)

    if tender_data["procuringEntity"] is not None:
        for org in tender_data["procuringEntity"]:
            procuring_link = {
                "id": uuid.uuid4(),
                "source": org["id"],
                "target": tender_data["id"],
            }
            procuring_node = {
                "name": org["name"],
                "id": org["id"],
                "identifier": org["identifier"],
                "additionalIdentifiers": org["additionalIdentifiers"],
                "address": org["address"],
                "contactPoint": org["contactPoint"],
                "roles": org["roles"],
                "details": org["details"],
            }
            nodes.append(procuring_node)
            links.append(procuring_link)

    for org in tender_data["tenderers"]:
        tenderer_link = {
            "id": uuid.uuid4(),
            "source": org["id"],
            "target": tender_data["id"],
        }
        tenderer_node = {
            "name": org["name"],
            "id": org["id"],
            "identifier": org["identifier"],
            "additionalIdentifiers": org["additionalIdentifiers"],
            "address": org["address"],
            "contactPoint": org["contactPoint"],
            "roles": org["roles"],
            "details": org["details"],
        }
        nodes.append(tenderer_node)
        links.append(tenderer_link)

    return {"nodes": nodes, "links": links}
