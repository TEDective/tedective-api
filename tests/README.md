<!--
SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# API Tests
This directory contains tests for the API and database

## Usage
From the main project directory:

`make api.test`

This will set up Docker containers with TerminusDB and TEDective API,
and then it will execute the tests that are stored in this directory.