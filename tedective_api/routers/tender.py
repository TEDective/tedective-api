# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import os

from fastapi import APIRouter, Depends, HTTPException, Query
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport
from gql.transport.exceptions import TransportProtocolError, TransportServerError

router = APIRouter()


@router.get(
    "/entities/tender/{id}",
    tags=["tenders"],
)
async def read_tender(id: str):
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]

    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})
    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:
        query = gql(
            """
            query getTender($id: String!) {
                Tender (filter: {id: {eq: $id}}) {
                    id
                    title
                    description
                    status
                    procuringEntity {
                        name
                        identifier {
                            scheme
                            legalName
                            uri
                        }
                        additionalIdentifiers {
                            scheme
                            legalName
                            uri
                        }
                        address {
                            countryCode
                            locality
                            postalCode
                            region
                            streetAddress
                        }
                        contactPoint {
                            name
                            email
                            telephone
                            faxNumber
                            url
                        }
                        roles
                        details
                    }
                    items {
                        description
                        classification {
                            scheme
                            description
                            uri
                        }
                        additionalClassifications {
                            scheme
                            description
                            uri
                        }
                        quantity
                        unit {
                            scheme
                            name
                            value {
                                amount
                                currency
                            }
                            uri
                        }
                    }
                    value {
                        amount
                        currency
                    }
                    minValue {
                        amount
                        currency
                    }
                    procurementMethod
                    procurementMethodDetails
                    procurementMethodRationale
                    mainProcurementCategory
                    additionalProcurementCategories
                    awardCriteria
                    awardCriteriaDetails
                    submissionMethod
                    submissionMethodDetails
                    tenderPeriod {
                        startDate
                        endDate
                        maxExtentDate
                        durationInDays
                    }
                    enquiryPeriod {
                        startDate
                        endDate
                        maxExtentDate
                        durationInDays
                    }
                    hasEnquiries
                    eligibilityCriteria
                    awardPeriod {
                        startDate
                        endDate
                        maxExtentDate
                        durationInDays
                    }
                    contractPeriod {
                        startDate
                        endDate
                        maxExtentDate
                        durationInDays
                    }
                    numberOfTenderers
                    tenderers {
                        name
                        identifier {
                            scheme
                            legalName
                            uri
                        }
                        additionalIdentifiers {
                            scheme
                            legalName
                            uri
                        }
                        address {
                            countryCode
                            locality
                            postalCode
                            region
                            streetAddress
                        }
                        contactPoint {
                            name
                            email
                            telephone
                            faxNumber
                            url
                        }
                        roles
                        details
                    }
                    documents {
                        documentType
                        title
                        description
                        url
                        datePublished
                        dateModified
                        format
                        language
                    }
                    milestones {
                        title
                        type
                        description
                        code
                        dueDate
                        dateMet
                        dateModified
                        status
                        documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                        }
                    }
                    amendments {
                        date
                        rationale
                        description
                        amendsReleaseID
                        releaseID
                        changes {
                            property
                            former_value
                        }
                    }
                    amendment {
                        date
                        rationale
                        description
                        amendsReleaseID
                        releaseID
                        changes {
                            property
                            former_value
                        }
                    }
                    crossBorderLaw
                }
            }
            """
        )
        params = {"id": id}
        try:
            result = await client.execute(query, variable_values=params)
            return result["Tender"][0]
        except IndexError or TransportProtocolError or TransportServerError:
            raise HTTPException(status_code=404, detail="Tender not found")
