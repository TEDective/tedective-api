# -*- coding: utf-8 -*-
from setuptools import setup

packages = \
['tedective_api', 'tedective_api.routers', 'tedective_api.routers.graph']

package_data = \
{'': ['*']}

install_requires = \
['aiohttp>=3.8.5,<4.0.0',
 'fastapi-versionizer>=0.1.6,<0.2.0',
 'fastapi>=0.101.1,<0.102.0',
 'fingerprints>=1.1.1,<2.0.0',
 'ftfy>=6.1.1,<7.0.0',
 'gql>=3.4.1,<4.0.0',
 'joblib>=1.3.2,<2.0.0',
 'lxml>=4.9.3,<5.0.0',
 'orjson>=3.9.4,<4.0.0',
 'phonenumbers>=8.13.18,<9.0.0',
 'python-dateutil>=2.8.2,<3.0.0',
 'rich>=13.5.2,<14.0.0',
 'terminusdb-client>=10.2.4,<11.0.0',
 'uvicorn>=0.23.2,<0.24.0']

setup_kwargs = {
    'name': 'tedective-api',
    'version': '0.1.0',
    'description': 'The API for the TEDective project based on FastAPI',
    'long_description': '<!--\nSPDX-FileCopyrightText: 2022 Free Software Foundation Europe <contact@fsfe.org>\n\nSPDX-License-Identifier: CC-BY-SA-4.0\n-->\n\n# TEDective API\n[![Black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)\n\nThe TEDective API makes use of the OCDS data stored in [TerminusDB](https://terminusdb.com), and allows to make requests for certain entities such as organizations, awards, contracts or releases. It can give responses in JSON as well as in [react-force-graph](https://github.com/vasturiano/react-force-graph#input-json-syntax)-ready format.\nPart of [TEDective Project](https://tedective.org).\n\n## Table of Contents\n\n- [TEDective API](#tedective-api)\n  - [Table of Contents](#table-of-contents)\n  - [Background](#background)\n  - [Install](#install)\n  - [Usage](#usage)\n  - [Maintainers](#maintainers)\n  - [Contributing](#contributing)\n  - [License](#license)\n\n## Background\n\nThe TEDective project aims to make European public procurement data from\nTenders Electronic Daily (TED) explorable for non-experts. This repository\ncontains all Python code used to build the API that is on top of TerminusDB\nwith (already converted to OCDS) TED data.\n\nPart of [TEDective Project](https://tedective.org)\n\n## Install\n[GNU Make](https://www.gnu.org/software/make/), [Docker](https://docker.com/) and [docker compose](https://docs.docker.com/compose/) are required.\n\nFor detailed instructions please refer to the [TEDective documentation](https://docs.tedective.org/api/quickstart.mdx).\n\n## Usage\n\n:construction: TODO: (probably put the reference to documentation)\n\n## Maintainers\n[@micgor32](https://github.com/micgor32)\n\n## Contributing\nTo get setup, you need [poetry](https://python-poetry.org/). Then you need to run:\n\n```bash\npoetry install\npoetry shell\n```\n\nWhen running on a local host, you would most likely want edit use [TEDective Parser](https://git.fsfe.org/TEDective/parser/), to setup up the TerminusDB instance with ingested data. By default API will use [.env](https://git.fsfe.org/TEDective/api/src/branch/main/.env) file to determine TerminusDB address and login credentials.\n\nMore details on how to provide custom address and credentials can be found in the [TEDective documentation](https://docs.tedective.org/api/quickstart.mdx).\n\nPRs are accepted.\n\nSmall note: If editing the README, please conform to the\n[standard-readme](https://github.com/RichardLitt/standard-readme)\nspecification.\n\n## License\n\nAGPL-3.0-or-later © 2023 Free Software Foundation Europe e.V.',
    'author': 'Free Software Foundation Europe e.V.',
    'author_email': 'tedective@fsfe.org',
    'maintainer': 'None',
    'maintainer_email': 'None',
    'url': 'None',
    'packages': packages,
    'package_data': package_data,
    'install_requires': install_requires,
    'python_requires': '>=3.11,<4.0',
}


setup(**setup_kwargs)
