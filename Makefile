# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: CC0-1.0

##
# TEDective API Makefile
# This Makefile contains make rules to ease working with TEDective API

.DEFAULT_GOAL := help

GREEN  = $(shell tput -Txterm setaf 2)
WHITE  = $(shell tput -Txterm setaf 7)
YELLOW = $(shell tput -Txterm setaf 3)
RESET  = $(shell tput -Txterm sgr0)

COMPOSE := docker compose

HELPME = \
	%help; \
	while(<>) { push @{$$help{$$2 // 'options'}}, [$$1, $$3] if /^([a-zA-Z\-\.]+)\s*:.*\#\#(?:@([a-zA-Z\-]+))?\s(.*)$$/ }; \
	for (sort keys %help) { \
	print "${WHITE}$$_:${RESET}\n"; \
	for (@{$$help{$$_}}) { \
	$$sep = " " x (20 - length $$_->[0]); \
	print "  ${YELLOW}$$_->[0]${RESET}$$sep${GREEN}$$_->[1]${RESET}\n"; \
	}; \
	print "\n"; }

help:
	@perl -e '$(HELPME)' $(MAKEFILE_LIST)
.PHONY: help


# -- Development (general)
dev: ##@development Start all containers and show their logs.
	@if [ -z "$(ADDRESS)" ] && [ -z "$(HEADERS)" ] && [ -z "$(KEY)"]; then \
		export USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`}; \
		docker compose up; \
  	else \
  	  	export USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`}; \
  	  	echo "Running with variables: ADDRESS=$(ADDRESS) HEADERS=$(HEADERS) KEY=$(TERMINUS_ADMIN_KEY)"; \
		DB_ADDRESS=$(ADDRESS) HEADERS=$(HEADERS) TERMINUS_ADMIN_KEY=$(KEY) docker compose up; \
	fi
.PHONY: dev

dev.up: ##@development Start all containers and detach.
	@if [ -z "$(ADDRESS)" ] && [ -z "$(HEADERS)" ] && [ -z "$(KEY)"]; then \
		export USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`}; \
		docker compose up --detach --no-recreate; \
  	else \
  	  	export USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`}; \
  	  	echo "Running with variables: ADDRESS=$(ADDRESS) HEADERS=$(HEADERS) KEY=$(TERMINUS_ADMIN_KEY)"; \
		DB_ADDRESS=$(ADDRESS) HEADERS=$(HEADERS) TERMINUS_ADMIN_KEY=$(KEY) docker compose up --detach --no-recreate; \
	fi
.PHONY: dev.up

dev.up.test: ##@development Start containers needed for testing and detach.
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} docker compose -f docker-compose.test.yml up --detach
.PHONY: dev.up.test

dev.down: ##@development Stop all containers.
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} docker compose down --remove-orphans
.PHONY: dev.down

dev.kill: ##@development Kill and subsequently remove all containers.
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} docker compose kill --remove-orphans
.PHONY: dev.kill

dev.rebuild: dev.down ##@development Stop and restart containers (but rebuild them this time)
	@if [ -z "$(ADDRESS)" ] && [ -z "$(HEADERS)" ] && [ -z "$(KEY)"]; then \
		export USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`}; \
		docker compose up --build --force-recreate; \
  	else \
  	  	export USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`}; \
  	  	echo "Running with variables: ADDRESS=$(ADDRESS) HEADERS=$(HEADERS) KEY=$(TERMINUS_ADMIN_KEY)"; \
		DB_ADDRESS=$(ADDRESS) HEADERS=$(HEADERS) TERMINUS_ADMIN_KEY=$(KEY) docker compose up --build --force-recreate; \
	fi
.PHONY: dev.rebuild

# -- API development
api.test: dev.up.test ##@api Test the TEDective API
	@USER_ID=$${USER_ID:-`id -u`} GROUP_ID=$${GROUP_ID:-`id -g`} docker compose exec -it tedective-api-test /bin/sh -c "pytest ./tests"
.PHONY: api.test

# end