# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import uuid
from datetime import datetime

import requests
from terminusdb_client.woqldataframe import result_to_df

from tedective_parser import schema

headers = {"Authorization": str(os.environ["HEADERS"])}
address = str(os.environ["ADDRESS_GQL"])


def test_document(setUp):
    client = setUp
    document1 = schema.Document(documentType="test")
    document2 = schema.Document(description="A random document.")
    document3 = schema.Document(datePublished=(datetime(2023, 1, 1)))

    client.insert_document([document1, document2, document3])
    result_1 = client.query_document({"@type": "Document", "documentType": "test"})
    result_1_value = result_to_df(result_1)

    result_2 = client.query_document(
        {"@type": "Document", "description": "A random document."}
    )
    result_2_value = result_to_df(result_2)

    result_3 = client.query_document(
        {"@type": "Document", "datePublished": "2023-01-01T00:00:00Z"}
    )
    result_3_value = result_to_df(result_3)

    assert result_1_value["documentType"].values[0] == "test"
    assert result_2_value["description"].values[0] == "A random document."
    assert result_3_value["datePublished"].values[0] == "2023-01-01T00:00:00Z"


# Commented out for now, requires list of instances of Change object, so those needs to be
# tested before testing Amendment
# def test_amendment(setUp):
#     client = setUp
#     amendment1 = schema.Amendment(description="An amendment.")
#     client.insert_document(amendment1)
#     result = client.query_document({'@type': 'Amendment', 'description': 'An amendment.'})
#     result_value = result_to_df(result)
#
#     assert result_value['description'].values[0] == "An amendment."
#     cleanup_testing_db()


def test_classification(setUp):
    client = setUp
    classification = schema.Classification(
        id="some_id_in_scheme", scheme="some_scheme", uri="https://some-uri.com"
    )
    client.insert_document(classification)
    result_1 = client.query_document(
        {"@type": "Classification", "id": "some_id_in_scheme"}
    )
    result_1_value = result_to_df(result_1)

    assert result_1_value["id"].values[0] == "some_id_in_scheme"
    assert result_1_value["scheme"].values[0] == "some_scheme"
    assert result_1_value["uri"].values[0] == "https://some-uri.com"


def test_identifier(setUp):
    client = setUp
    identifier = schema.Identifier(id="some_org_id", scheme="some_org_scheme")
    client.insert_document(identifier)
    result_1 = client.query_document({"@type": "Identifier", "id": "some_org_id"})
    result_1_value = result_to_df(result_1)

    assert result_1_value["id"].values[0] == "some_org_id"
    assert result_1_value["scheme"].values[0] == "some_org_scheme"


def test_address(setUp):
    client = setUp
    address = schema.Address(street="Am Musterplatz 1")
    client.insert_document(address)
    result = client.query_document({"@type": "Address", "street": "Am Musterplatz 1"})
    result_value = result_to_df(result)

    assert result_value["street"].values[0] == "Am Musterplatz 1"


def test_contact_point(setUp):
    client = setUp
    contact_point = schema.ContactPoint(email="test@test.com")
    client.insert_document(contact_point)
    result = client.query_document({"@type": "ContactPoint", "email": "test@test.com"})
    result_value = result_to_df(result)

    assert result_value["email"].values[0] == "test@test.com"


def test_value(setUp):
    client = setUp
    value = schema.Value(amount=123.12)
    client.insert_document(value)
    result = client.query_document({"@type": "Value", "amount": 123.12})
    result_value = result_to_df(result)

    assert result_value["amount"].values[0] == 123.12


def test_period(setUp):
    client = setUp
    period = schema.Period(startDate=datetime(2022, 12, 2))
    client.insert_document(period)
    result = client.query_document(
        {"@type": "Period", "startDate": "2022-12-02T00:00:00Z"}
    )
    result_value = result_to_df(result)

    assert result_value["startDate"].values[0] == "2022-12-02T00:00:00Z"


# def test_related_process(setUp):
#     client = setUp
#     toAdd = set()
#     toAdd.add("prior")
#     toAdd.add("parent")
#     related_process = schema.RelatedProcess(relationship=toAdd)
#     client.insert_document(related_process)
#     result = client.query_document({'@type': 'RelatedProcess', 'relationship': {'$in': list(toAdd)}})
#     result_value = result_to_df(result)
#
#     assert result_value['relationship'].values[0] == toAdd
#     cleanup_testing_db()


def test_milestone(setUp):
    client = setUp
    milestone = schema.Milestone(title="Finish the API!")
    client.insert_document(milestone)
    result = client.query_document({"@type": "Milestone", "title": "Finish the API!"})
    result_value = result_to_df(result)

    assert result_value["title"].values[0] == "Finish the API!"


def test_budget(setUp):
    client = setUp
    budget = schema.Budget(
        id=123,
        amount=schema.Value(amount=123.12, currency=schema.Currency.EUR),
        description="This is a budget.",
        project="Sample Project",
    )
    client.insert_document(budget)
    query = {
        "query": """\nquery{
	Budget  (filter: {id: {eq: "123"}}) {
    description
    _id
    project
    amount {
        amount
        currency
    }
  }
}
"""
    }

    response = requests.post(address, json=query, headers=headers)
    result = response.json()

    assert result["data"]["Budget"][0]["project"] == "Sample Project"
    assert result["data"]["Budget"][0]["description"] == "This is a budget."
    assert result["data"]["Budget"][0]["amount"]["amount"] == 123.12
    assert result["data"]["Budget"][0]["amount"]["currency"] == "EUR"


def test_organization_insertion(setUp):
    client = setUp
    organization = schema.Organization(
        name="Evil Corp.",
        address=schema.Address(street="Evil Boulevard 666"),
        identifier=schema.Identifier(scheme="some_scheme", id="some_id_in_scheme"),
        additionalIdentifiers=set(
            [
                schema.Identifier(scheme="some_scheme", id="some_id_in_scheme"),
                schema.Identifier(scheme="some_other_scheme", id="some_id_in_scheme"),
            ]
        ),
        roles=set(["buyer", "procuringEntity", "funder"]),
    )

    client.insert_document(organization)
    query = {
        "query": """\nquery{
    	Organization (filter: {name: {eq: "Evil Corp."}}) {
        name
        address {
            street
        }
        identifier {
            scheme
        }
        additionalIdentifiers {
      	    scheme
        }
        roles
      }
    }
    """
    }

    response = requests.post(address, json=query, headers=headers)
    result = response.json()

    assert result["data"]["Organization"][0]["name"] == "Evil Corp."
    assert (
        result["data"]["Organization"][0]["address"]["street"] == "Evil Boulevard 666"
    )
    assert result["data"]["Organization"][0]["identifier"]["scheme"] == "some_scheme"
    assert (
        result["data"]["Organization"][0]["additionalIdentifiers"][0]["scheme"]
        == "some_other_scheme"
        or result["data"]["Organization"][0]["additionalIdentifiers"][0]["scheme"]
        == "some_scheme"
    )
    assert result["data"]["Organization"][0]["roles"][1] == "funder"


def test_unit(setUp):
    client = setUp
    unit = schema.Unit(
        value=schema.Value(amount=123.12, currency=schema.Currency.EUR),
        name="Some unit",
    )

    client.insert_document(unit)

    query = {
        "query": """\nquery{
        	Unit (filter: {name: {eq: "Some unit"}}) {
        	    value {
        	        amount
        	        currency
        	    }
        	    name
        	}
        }
    """
    }

    response = requests.post(address, json=query, headers=headers)
    result = response.json()

    assert result["data"]["Unit"][0]["value"]["amount"] == 123.12


def test_item(setUp):
    client = setUp

    item = schema.Item(
        description="Some item",
        additionalClassifications=set(
            [
                schema.Classification(scheme="some_scheme", id="some_id_in_scheme"),
                schema.Classification(
                    scheme="another_scheme", id="another_id_in_scheme"
                ),
            ]
        ),
        quantity=12.2,
        unit=schema.Unit(name="Metric tons"),
    )

    client.insert_document(item)

    query = {
        "query": """\nquery{
            Item (filter: {quantity: {eq: 12.2}}) {
                description
                additionalClassifications {
                    scheme
                }
                quantity
                unit {
                    name
                }
            }
        }
    """
    }

    response = requests.post(url=address, json=query, headers=headers)
    result = response.json()

    assert result["data"]["Item"][0]["description"] == "Some item"
    assert result["data"]["Item"][0]["unit"]["name"] == "Metric tons"
    assert (
        result["data"]["Item"][0]["additionalClassifications"][0]["scheme"]
        == "some_scheme"
        or result["data"]["Item"][0]["additionalClassifications"][0]["scheme"]
        == "another_scheme"
    )


def test_planning_insertion(setUp):
    client = setUp
    planning = schema.Planning(
        rationale="some rationale",
        budget=schema.Budget(
            amount=schema.Value(amount=123.12, currency=schema.Currency.EUR)
        ),
    )
    client.insert_document(planning)

    query = {
        "query": """\nquery{
                    Planning (filter: {rationale: {eq: "some rationale"}}) {
                        rationale
                        budget {
                            amount {
                                amount
                                currency
                            }
                        }
                    }
                }
            """
    }

    response = requests.post(url=address, json=query, headers=headers)
    result = response.json()

    assert result["data"]["Planning"][0]["rationale"] == "some rationale"


def test_tender_insertion(setUp):
    client = setUp
    tender = schema.Tender(
        id="ocid-test-fsfe12345",
        # tenderers=[
        #     models.Organization(name="Some Corp"),
        #     models.Organization(name="Other Corp"),
        # ],
        documents=set(
            [
                schema.Document(title="Some Doc"),
                schema.Document(title="Other Doc"),
            ]
        ),
        tenderPeriod=schema.Period(durationInDays=11),
        awardPeriod=schema.Period(durationInDays=123),
        enquiryPeriod=schema.Period(durationInDays=12),
        contractPeriod=schema.Period(durationInDays=111),
        value=schema.Value(amount=123.12, currency=schema.Currency.EUR),
        minValue=schema.Value(amount=1.0, currency=schema.Currency.EUR),
        milestones=set([schema.Milestone(title="some milestone")]),
        amendments=set(
            [
                schema.Amendment(
                    rationale="some rationale", changes=[schema.Change(property="Test")]
                )
            ]
        ),
        submissionMethod=set(["mail"]),
        mainProcurementCategory=schema.MainProcurementCategory.goods,
        additionalProcurementCategories=set(["test", "test2"]),
    )

    client.insert_document(tender)

    query = {
        "query": """\nquery{
            Tender (filter: {id: {eq: "ocid-test-fsfe12345"}}) {
                documents {
                    title
                }
                tenderPeriod {
                    durationInDays
                }
                awardPeriod {
                    durationInDays
                }
                enquiryPeriod {
                    durationInDays
                }
                contractPeriod {
                    durationInDays
                }
                value {
                    amount
                    currency
                }
                minValue {
                    amount
                    currency
                }
                milestones {
                    title
                }
                amendments {
                    rationale
                }
                submissionMethod
                mainProcurementCategory
                additionalProcurementCategories
            }
        }
    """
    }

    response = requests.post(url=address, json=query, headers=headers)
    result = response.json()

    assert result["data"]["Tender"][0]["value"]["amount"] == 123.12
    assert (
        result["data"]["Tender"][0]["documents"][0]["title"] == "Other Doc"
        or result["data"]["Tender"][0]["documents"][0]["title"] == "Some Doc"
    )
    assert result["data"]["Tender"][0]["tenderPeriod"]["durationInDays"] == "11"
    assert result["data"]["Tender"][0]["awardPeriod"]["durationInDays"] == "123"
    assert result["data"]["Tender"][0]["enquiryPeriod"]["durationInDays"] == "12"
    assert result["data"]["Tender"][0]["contractPeriod"]["durationInDays"] == "111"
    # TODO: few other assertions

    # with sync_session as session:
    #     session.add(tender)
    #     session.commit()
    #     session.refresh(tender)
    #     # Remember that Tender.id is the OCID
    #     assert tender.id == "ocid-test-fsfe12345"
    #     assert type(tender.uid) == uuid.UUID
    #     # assert "Other Corp" in [t.name for t in tender.tenderers]
    #     assert "Other Doc" in [d["title"] for d in tender.documents]
    #     assert tender.awardPeriod["durationInDays"] == 123
    #     assert tender.enquiryPeriod["durationInDays"] == 12
    #     assert "some milestone" in [m["title"] for m in tender.milestones]
    #     assert "some rationale" in [a["rationale"] for a in tender.amendments]
    #     assert "mail" in tender.submissionMethod
    #     assert "test2" in tender.additionalProcurementCategories
    #     assert tender.mainProcurementCategory == models.MainProcurementCategory("goods")
    #     assert tender.mainProcurementCategory.name == "goods"


def test_award_insertion(setUp):
    client = setUp

    award = schema.Award(
        title="Best in show",
        status=schema.AwardStatus.active,
        items=set(
            [
                schema.Item(description="Some item"),
                schema.Item(description="Another item"),
            ]
        ),
        documents=set(
            [
                schema.Document(title="Some Doc"),
                schema.Document(title="Other Doc"),
            ]
        ),
        contractPeriod=schema.Period(endDate=datetime(2022, 12, 1, 23, 59, 59)),
    )

    client.insert_document(award)

    query = {
        "query": """\nquery{
    	        Award (filter: {title: {eq: "Best in show"}}) {
    	            title
    	            status
    	            items {
    	                description
    	            }
    	            documents {
    	                title
    	            }
    	            contractPeriod {
    	                durationInDays
    	            }
                }
            }
    """
    }

    response = requests.post(url=address, json=query, headers=headers)
    result = response.json()

    assert result["data"]["Award"][0]["title"] == "Best in show"
    assert result["data"]["Award"][0]["status"] == "active"
    assert (
        result["data"]["Award"][0]["items"][0]["description"] == "Some item"
        or result["data"]["Award"][0]["items"][0]["description"] == "Another item"
    )
    assert (
        result["data"]["Award"][0]["documents"][0]["title"] == "Some Doc"
        or result["data"]["Award"][0]["documents"][0]["title"] == "Other Doc"
    )


#     with sync_session as session:
#         session.add(award)
#         session.commit()
#         session.refresh(award)
#         assert type(award.id) == uuid.UUID
#         assert award.status == models.AwardStatus("active")
#         assert "Other Doc" in [d["title"] for d in award.documents]
#         assert award.contractPeriod["endDate"] == "2022-12-01T23:59:59"


def test_implementation_insertion(setUp):
    client = setUp
    implementation = schema.Implementation(
        transactions=set(
            [
                schema.Transaction(source="https://some.org"),
                schema.Transaction(source="https://other.org"),
            ]
        ),
        milestones=set(
            [
                schema.Milestone(title="Some milestone"),
                schema.Milestone(title="Other milestone"),
            ]
        ),
        documents=set(
            [
                schema.Document(title="Some document"),
                schema.Document(title="Other document"),
            ]
        ),
    )
    client.insert_document(implementation)

    query = {
        "query": """\nquery{
            Implementation {
                transactions {
                    source
                }
                milestones {
                    title
                }
                documents {
                    title
                }
            }
        }
    """
    }

    response = requests.post(url=address, json=query, headers=headers)
    result = response.json()

    assert (
        result["data"]["Implementation"][0]["transactions"][0]["source"]
        == "https://some.org"
        or result["data"]["Implementation"][0]["transactions"][0]["source"]
        == "https://other.org"
    )
    assert (
        result["data"]["Implementation"][0]["milestones"][0]["title"]
        == "Some milestone"
        or result["data"]["Implementation"][0]["milestones"][0]["title"]
        == "Other milestone"
    )
    assert (
        result["data"]["Implementation"][0]["documents"][0]["title"] == "Some document"
        or result["data"]["Implementation"][0]["documents"][0]["title"]
        == "Other document"
    )


#     with sync_session as session:
#         session.add(implementation)
#         session.commit()
#         session.refresh(implementation)
#         assert implementation.uid == 1
#         assert "https://some.org" in [t["source"] for t in implementation.transactions]
#         assert "Other milestone" in [m["title"] for m in implementation.milestones]
#         assert "Other document" in [d["title"] for d in implementation.documents]
#
#
def test_contract_insertion(setUp):
    client = setUp
    contract = schema.Contract(
        id=123,
        awardID=123,
        dateSigned=str(datetime(2022, 12, 2)),
        period=schema.Period(durationInDays=123),
        value=schema.Value(amount=123.12, currency=schema.Currency.EUR),
        items=set(
            [
                schema.Item(description="an item"),
                schema.Item(description="another item"),
            ]
        ),
        milestones=set(
            [
                schema.Milestone(description="a milestone"),
                schema.Milestone(description="another milestone"),
            ]
        ),
    )

    client.insert_document(contract)

    query = {
        "query": """\nquery{
            Contract (filter: {id: {eq: "123"}}) {
                dateSigned
                awardID
                period {
                    durationInDays
                }
                value {
                    amount
                    currency
                }
                items {
                    description
                }
                milestones {
                    description
                }
            }
        }
    """
    }

    response = requests.post(url=address, json=query, headers=headers)
    result = response.json()

    assert result["data"]["Contract"][0]["dateSigned"] == "2022-12-02 00:00:00"
    assert result["data"]["Contract"][0]["awardID"] == "123"
    assert result["data"]["Contract"][0]["period"]["durationInDays"] == "123"
    assert (
        result["data"]["Contract"][0]["items"][0]["description"] == "an item"
        or result["data"]["Contract"][0]["items"][0]["description"] == "another item"
    )
    assert (
        result["data"]["Contract"][0]["milestones"][0]["description"] == "a milestone"
        or result["data"]["Contract"][0]["milestones"][0]["description"]
        == "another milestone"
    )


def test_release_insertion(setUp):
    client = setUp
    release = schema.Release(
        ocid="ocid-fsfe12345",
        tag=[schema.Tag.planning, schema.Tag.planningUpdate],
        date=datetime(2022, 12, 2),
        initiationType=schema.InitiationType.tender,
        parties=set(
            [
                schema.Organization(name="Evil Corp"),
                schema.Organization(name="Strong State Org"),
            ]
        ),
        buyer=set([schema.Organization(name="Strong State Org")]),
        planning=schema.Planning(
            budget=schema.Budget(
                amount=schema.Value(amount=123.12, currency=schema.Currency.EUR)
            )
        ),
        tender=schema.Tender(
            title="Some Tender", procuringEntity=schema.Organization(name="ProcurPlus")
        ),
        awards=set([schema.Award(id=str(uuid.uuid4()), title="Some Award")]),
        contracts=set(
            [
                schema.Contract(
                    id=str(uuid.uuid4()), awardID="123", title="Some Contract"
                )
            ]
        ),
        relatedProcesses=set(
            [
                schema.RelatedProcess(
                    title="Some other release",
                    relationships=["prior", "other"],
                )
            ]
        ),
    )
    client.insert_document(release)

    query = {
        "query": """\nquery{
            Release (filter: {ocid: {eq: "ocid-fsfe12345"}}) {
                ocid
                tag
                initiationType
                parties {
                    name
                }
                buyer {
                    name
                }
                planning {
                    budget {
                        amount {
                            amount
                            currency
                        }
                    }
                }
                tender {
                    title
                    procuringEntity {
                        name
                    }
                }
                awards {
                    title
                }
                contracts {
                    awardID
                    title
                }
                relatedProcesses {
                    title
                    relationship
                }
            }
        }
    """
    }

    response = requests.post(url=address, json=query, headers=headers)
    result = response.json()

    assert result["data"]["Release"][0]["ocid"] == "ocid-fsfe12345"
    assert result["data"]["Release"][0]["initiationType"] == "tender"
    assert (
        result["data"]["Release"][0]["tag"][0] == "planning"
        or result["data"]["Release"][0]["tag"][0] == "planningUpdate"
    )
    assert (
        result["data"]["Release"][0]["parties"][0]["name"] == "Evil Corp"
        or result["data"]["Release"][0]["parties"][0]["name"] == "Strong State Org"
    )
    assert result["data"]["Release"][0]["tender"]["title"] == "Some Tender"
    assert result["data"]["Release"][0]["awards"][0]["title"] == "Some Award"
