# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os
import uuid

from fastapi import APIRouter, Depends, HTTPException
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport
from gql.transport.exceptions import TransportProtocolError, TransportServerError

router = APIRouter()


@router.get("/entities/contract/{id}", tags=["contracts"])
async def read_contract(
    id: uuid.UUID,
):
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]

    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})
    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:

        query = gql(
            """
        query getContract($id: String!)  {
            Contract(filter: {id: {eq: $id}}) {
                id
                title
                awardID
                description
                status
                period {
                    startDate
                    endDate
                    maxExtentDate
                    durationInDays
                }
                value {
                    amount
                    currency
                }
                items {
                    id
                    description
                    classification {
                        scheme
                        description
                        uri
                    }
                    additionalClassifications {
                        scheme
                        description
                        uri
                    }
                    quantity
                    unit {
                        scheme
                        name
                        value {
                          amount
                          currency
                        }
                        uri
                    }
                }
                dateSigned
                documents {
                    documentType
                    title
                    description
                    url
                    datePublished
                    dateModified
                    format
                    language
                }
                implementation {
                    transactions {
                        source
                        date
                        value {
                            amount
                            currency
                        }
                        payer {
                            name
                            identifier {
                                scheme
                                legalName
                                uri
                            }
                            additionalIdentifiers {
                                scheme
                                legalName
                                uri
                            }
                            address {
                                countryCode
                                locality
                                postalCode
                                region
                                streetAddress 
                            }
                            contactPoint {
                                name
                                email
                                telephone
                                faxNumber
                                url
                            }
                            roles
                            details
                        }
                        payee {
                            name
                            identifier {
                                scheme
                                legalName
                                uri
                            }
                            additionalIdentifiers {
                                scheme
                                legalName
                                uri
                            }
                            address {
                                countryCode
                                locality
                                postalCode
                                region
                                streetAddress 
                            }
                            contactPoint {
                                name
                                email
                                telephone
                                faxNumber
                                url
                            }
                            roles
                            details
                        }
                        uri
                        amount {
                            amount
                            currency
                        }
                        providerOrganization {
                            scheme
                            legalName
                            uri
                        }
                        receiverOrganization {
                            scheme
                            legalName
                            uri
                        }
                    }
                    milestones {
                        title
                        type
                        description
                        code
                        dueDate
                        dateMet
                        dateModified
                        status
                        documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                        }
                    }
                    documents {
                        documentType
                        title
                        description
                        url
                        datePublished
                        dateModified
                        format
                        language
                    }
                }
                relatedProcesses {
                    relationship
                    title
                    scheme
                    identifier
                    uri
                }
                milestones {
                    title
                    type
                    description
                    code
                    dueDate
                    dateMet
                    dateModified
                    status
                    documents {
                        documentType
                        title
                        description
                        url
                        datePublished
                        dateModified
                        format
                        language
                    }
                }
                amendments {
                    date
                    rationale
                    description
                    amendsReleaseID
                    releaseID
                    changes {
                        property
                        former_value
                    }
                }
                amendment {
                    date
                    rationale
                    description
                    amendsReleaseID
                    releaseID
                    changes {
                        property
                        former_value
                    }
                }
            }
        }
    """
        )

        params = {"id": str(id)}
        try:
            result = await client.execute(query, variable_values=params)
            return result["Contract"][0]
        except IndexError or TransportProtocolError or TransportServerError:
            raise HTTPException(status_code=404, detail="Contract not found")
