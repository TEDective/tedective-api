# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import uuid
from datetime import datetime

import requests

from tedective_parser import schema

headers = {"accept": "application/json"}
address = "http://0.0.0.0:9000/latest"


def test_read_award_success(setUp):
    client = setUp
    award = schema.Award(
        id="f9976094-794a-4ccc-8e4e-f688b5884c42",
        title="Best in show",
        status=schema.AwardStatus.active,
        items=set(
            [
                schema.Item(description="Some item"),
                schema.Item(description="Another item"),
            ]
        ),
        documents=set(
            [
                schema.Document(title="Some Doc"),
                schema.Document(title="Other Doc"),
            ]
        ),
        contractPeriod=schema.Period(endDate=datetime(2022, 12, 1, 23, 59, 59)),
    )

    client.insert_document(award)

    response = requests.get(
        url=str(address + "/award/f9976094-794a-4ccc-8e4e-f688b5884c42")
    )
    result = response.json()

    assert result["title"] == "Best in show"
    assert result["status"] == "active"
    assert (
        result["items"][0]["description"] == "Some item"
        or result["items"][0]["description"] == "Another item"
    )
    assert (
        result["documents"][0]["title"] == "Some Doc"
        or result["documents"][0]["title"] == "Other Doc"
    )


def test_award_failure():
    response = requests.get(url=str(address + "/award/" + str(uuid.uuid4())))
    result = response.json()
    print(result)

    assert result["detail"] == "Award not found"


def test_read_tenders(setUp):
    client = setUp
    t1 = schema.Tender(
        id="ocid-test-fsfe12345",
        documents=set(
            [
                schema.Document(title="Some Doc"),
                schema.Document(title="Other Doc"),
            ]
        ),
        tenderPeriod=schema.Period(durationInDays=11),
        awardPeriod=schema.Period(durationInDays=123),
        enquiryPeriod=schema.Period(durationInDays=12),
        contractPeriod=schema.Period(durationInDays=111),
        value=schema.Value(amount=123.12, currency=schema.Currency.EUR),
        minValue=schema.Value(amount=1.0, currency=schema.Currency.EUR),
        milestones=set([schema.Milestone(title="some milestone")]),
        amendments=set(
            [
                schema.Amendment(
                    rationale="some rationale", changes=[schema.Change(property="Test")]
                )
            ]
        ),
        submissionMethod=set(["mail"]),
        mainProcurementCategory=schema.MainProcurementCategory.goods,
        additionalProcurementCategories=set(["test", "test2"]),
    )
    t2 = schema.Tender(
        id="ocid-test-fsfe123456",
        documents=set(
            [
                schema.Document(title="Some Doc"),
                schema.Document(title="Other Doc"),
            ]
        ),
        tenderPeriod=schema.Period(durationInDays=11),
        awardPeriod=schema.Period(durationInDays=123),
        enquiryPeriod=schema.Period(durationInDays=12),
        contractPeriod=schema.Period(durationInDays=111),
        value=schema.Value(amount=123.12, currency=schema.Currency.EUR),
        minValue=schema.Value(amount=1.0, currency=schema.Currency.EUR),
        milestones=set([schema.Milestone(title="some milestone")]),
        amendments=set(
            [
                schema.Amendment(
                    rationale="some rationale", changes=[schema.Change(property="Test")]
                )
            ]
        ),
        submissionMethod=set(["mail"]),
        mainProcurementCategory=schema.MainProcurementCategory.goods,
        additionalProcurementCategories=set(["test", "test2"]),
    )
    client.insert_document([t1, t2])

    response = requests.get(url=str(address + "/tender/ocid-test-fsfe12345"))
    result = response.json()

    assert result["value"]["amount"] == 123.12
    assert (
        result["documents"][0]["title"] == "Other Doc"
        or result["documents"][0]["title"] == "Some Doc"
    )
    assert result["tenderPeriod"]["durationInDays"] == "11"
    assert result["awardPeriod"]["durationInDays"] == "123"
    assert result["enquiryPeriod"]["durationInDays"] == "12"
    assert result["contractPeriod"]["durationInDays"] == "111"


def test_tender_failure():
    response = requests.get(url=str(address + "/tender/" + str(uuid.uuid4())))
    result = response.json()

    assert result["detail"] == "Tender not found"


def test_read_contract(setUp):
    client = setUp
    id = uuid.uuid4()
    c = schema.Contract(
        id=str(id),
        awardID=123,
        dateSigned=str(datetime(2022, 12, 2)),
        period=schema.Period(durationInDays=123),
        value=schema.Value(amount=123.12, currency=schema.Currency.EUR),
        items=set(
            [
                schema.Item(description="an item"),
                schema.Item(description="another item"),
            ]
        ),
        milestones=set(
            [
                schema.Milestone(description="a milestone"),
                schema.Milestone(description="another milestone"),
            ]
        ),
    )
    client.insert_document(c)

    response = requests.get(url=str(address + "/contract/" + str(id)))
    result = response.json()

    assert result["dateSigned"] == "2022-12-02 00:00:00"
    assert result["awardID"] == "123"
    assert result["period"]["durationInDays"] == "123"
    assert (
        result["items"][0]["description"] == "an item"
        or result["items"][0]["description"] == "another item"
    )
    assert (
        result["milestones"][0]["description"] == "a milestone"
        or result["milestones"][0]["description"] == "another milestone"
    )


def test_contract_failure():
    response = requests.get(url=str(address + "/contract/" + str(uuid.uuid4())))
    result = response.json()

    assert result["detail"] == "Contract not found"


async def test_read_release(setUp):
    client = setUp

    r = schema.Release(
        ocid="ocid-fsfe12345",
        tag=[schema.Tag.planning, schema.Tag.planningUpdate],
        date=datetime(2022, 12, 2),
        initiationType=schema.InitiationType.tender,
        parties=set(
            [
                schema.Organization(name="Evil Corp"),
                schema.Organization(name="Strong State Org"),
            ]
        ),
        buyer=set([schema.Organization(name="Strong State Org")]),
        planning=schema.Planning(
            budget=schema.Budget(
                amount=schema.Value(amount=123.12, currency=schema.Currency.EUR)
            )
        ),
        tender=schema.Tender(
            title="Some Tender", procuringEntity=schema.Organization(name="ProcurPlus")
        ),
        awards=set([schema.Award(id=str(uuid.uuid4()), title="Some Award")]),
        contracts=set(
            [
                schema.Contract(
                    id=str(uuid.uuid4()), awardID="123", title="Some Contract"
                )
            ]
        ),
        relatedProcesses=set(
            [
                schema.RelatedProcess(
                    title="Some other release",
                    relationships=["prior", "other"],
                )
            ]
        ),
    )
    client.insert_document(r)

    response = requests.get(str(address + "/release/" + "ocid-fsfe12345"))
    result = response.json()
    print(result)

    assert result["ocid"] == "ocid-fsfe12345"
    assert result["initiationType"] == "tender"
    assert result["tag"][0] == "planning" or result["tag"][0] == "planningUpdate"
    assert (
        result["parties"][0]["name"] == "Evil Corp"
        or result["parties"][0]["name"] == "Strong State Org"
    )
    assert result["tender"]["title"] == "Some Tender"
    assert result["awards"][0]["title"] == "Some Award"


def test_release_failure():
    response = requests.get(url=str(address + "/release/" + str(uuid.uuid4())))
    result = response.json()

    assert result["detail"] == "Release not found"


def test_organization_read(setUp):
    client = setUp
    id = uuid.uuid4()
    organization = schema.Organization(
        id=str(id),
        name="Evil Corp.",
        address=schema.Address(street="Evil Boulevard 666"),
        identifier=schema.Identifier(scheme="some_scheme", id="some_id_in_scheme"),
        additionalIdentifiers=set(
            [
                schema.Identifier(scheme="some_scheme", id="some_id_in_scheme"),
                schema.Identifier(scheme="some_other_scheme", id="some_id_in_scheme"),
            ]
        ),
        roles=set(["buyer", "procuringEntity", "funder"]),
    )

    client.insert_document(organization)

    response = requests.get(url=str(address + "/organization/" + str(id)))
    result = response.json()

    assert result["name"] == "Evil Corp."
    assert result["address"]["street"] == "Evil Boulevard 666"
    assert result["identifier"]["scheme"] == "some_scheme"
    assert (
        result["additionalIdentifiers"][0]["scheme"] == "some_other_scheme"
        or result["additionalIdentifiers"][0]["scheme"] == "some_scheme"
    )
    assert result["roles"][1] == "funder"


def test_organization_failure():
    response = requests.get(url=str(address + "/organization/" + str(uuid.uuid4())))
    result = response.json()

    assert result["detail"] == "Organization not found"


# Tests for the graph endpoints
# def test_contract_graph(setUp):
#     client = setUp
#     id = uuid.uuid4()
#     c = schema.Contract(
#         id=str(id),
#         awardID=123,
#         dateSigned=str(datetime(2022, 12, 2)),
#         period=schema.Period(durationInDays=123),
#         value=schema.Value(amount=123.12, currency=schema.Currency.EUR),
#         items=set(
#             [
#                 schema.Item(description="an item"),
#                 schema.Item(description="another item"),
#             ]
#         ),
#         milestones=set(
#             [
#                 schema.Milestone(description="a milestone"),
#                 schema.Milestone(description="another milestone"),
#             ]
#         ),
#     )
#     client.insert_document(c)
#
#     response = requests.get(url=str(address + "/graphql/contract/" + str(id)))
#     result = response.json()
#     print(result)
#
#     assert result["nodes"]["id"] == str(id)


# @pytest.mark.asyncio
# async def test_read_releases(async_session: AsyncSession, async_client: AsyncClient):
#     r1 = schema.Release(
#         ocid="ocid-asdf-1",
#         initiationType=schema.InitiationType("tender"),
#         tag=["award", "contract"],
#         date=datetime(2022, 12, 2),
#         tender=schema.Tender(
#             value=schema.Value(amount=123.12, currency=schema.Currency("EUR"))
#         ),
#         buyer=schema.Organization(
#             name="Strong State Org.",
#             address=schema.Address(streetAddress="some stree"),
#             contactPoint=schema.ContactPoint(name="Functionary 1"),
#             details={"url": "test.com"},
#             roles=["buyer"],
#         ),
#         parties=[
#             schema.Organization(name="Some corpo"),
#             schema.Organization(name="another corpo"),
#         ],
#     )
#     r2 = schema.Release(
#         ocid="ocid-asdf-1",
#         initiationType="tender",
#         date=datetime(2022, 12, 2),
#         tender=schema.Tender(value=schema.Value(amount=23.12, currency="EUR")),
#     )
#     r3 = schema.Release(
#         ocid="ocid-qwer-1",
#         initiationType="tender",
#         date=datetime(2022, 12, 2),
#         tender=schema.Tender(value=schema.Value(amount=123.12, currency="EUR")),
#     )
#
#     async_session.add_all([r1, r2, r3])
#     await async_session.commit()
#
#     response = await async_client.get(f"/latest/releases/ocid-asdf-1")
#     data = response.json()
#     assert len(data) == 2
#
#     response = await async_client.get(f"/latest/releases/ocid-qwer-1")
#     data = response.json()
#     assert len(data) == 1
#
#
# @pytest.mark.asyncio
# async def test_read_releases_for_organisation(
#     async_session: AsyncSession, async_client: AsyncClient
# ):
#     strong_state_org = schema.Organization(
#         name="Strong State Org.",
#         address=schema.Address(streetAddress="some stree"),
#         contactPoint=schema.ContactPoint(name="Functionary 1"),
#         details={"url": "test.com"},
#         roles=["buyer"],
#     )
#     r1 = schema.Release(
#         ocid="ocid-asdf-1",
#         initiationType=schema.InitiationType("tender"),
#         tag=["award", "contract"],
#         date=datetime(2022, 12, 2),
#         tender=schema.Tender(
#             value=schema.Value(amount=123.12, currency=schema.Currency("EUR"))
#         ),
#         buyer=strong_state_org,
#         parties=[
#             schema.Organization(
#                 id=uuid.uuid5(
#                     uuid.NAMESPACE_URL,
#                     "supplier1",
#                 ),
#                 name="Some corpo",
#                 roles=["buyer"],
#             ),
#             schema.Organization(
#                 id=uuid.uuid5(
#                     uuid.NAMESPACE_URL,
#                     "supplier2",
#                 ),
#                 name="another corpo",
#                 roles=["buyer"],
#             ),
#         ],
#     )
#
#     r2 = schema.Release(
#         ocid="ocid-asdf-1",
#         initiationType="tender",
#         date=datetime(2022, 12, 2),
#         tender=schema.Tender(value=schema.Value(amount=23.12, currency="EUR")),
#     )
#     r3 = schema.Release(
#         ocid="ocid-qwer-1",
#         initiationType="tender",
#         date=datetime(2022, 12, 2),
#         tender=schema.Tender(value=schema.Value(amount=123.12, currency="EUR")),
#     )
#
#     async_session.add_all([r1, r2, r3])
#     await async_session.commit()
#
#     response = await async_client.get(f"/latest/releases/ocid-asdf-1")
#     data = response.json()
#     assert len(data) == 2
#
#     response = await async_client.get(f"/latest/releases/ocid-qwer-1")
#     data = response.json()
#     assert len(data) == 1
