# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import uuid

from fastapi import APIRouter, Depends, HTTPException

from tedective_api.routers.award import read_award
from tedective_api.routers.contract import read_contract

router = APIRouter()


@router.get("/graph/contract/{id}", tags=["contracts"])
async def get_contract(
    id: uuid.UUID,
):
    response = await read_contract(id)

    # Data needed to create Award node and link it to awardID from contract
    award_data = await read_award(response["awardID"])
    graph = convert_response_to_graph(response, award_data)
    return graph


def convert_response_to_graph(contract_data, award_data):
    nodes = []
    links = []

    award_node = {
        "name": award_data["title"],
        "id": award_data["id"],
        "description": award_data["description"],
        "status": award_data["status"],
        "date": award_data["date"],
        "value": award_data["value"],
        "contractPeriod": award_data["contractPeriod"],
        "documents": award_data["documents"],
        "amendments": award_data["amendments"],
        "amendment": award_data["amendment"],
    }
    nodes.append(award_node)

    contract_node = {
        "title": contract_data["title"],
        "id": contract_data["id"],
        "awardID": contract_data[
            "awardID"
        ],  # TODO: link create a award node based on that
        "description": contract_data["description"],
        "period": contract_data["period"],
        "value": contract_data["value"],
        "dateSigned": contract_data["dateSigned"],
        "documents": contract_data["documents"],
        "implementation": contract_data["implementation"],
        "items": contract_data["items"],
        "relatedProcesses": contract_data["relatedProcesses"],
        "milestones": contract_data["milestones"],
        "amendments": contract_data["amendments"],
        "amendment": contract_data["amendment"],
    }

    nodes.append(contract_node)

    award_link = {
        "id": uuid.uuid4(),
        "source": contract_data["id"],
        "target": award_data["id"],
    }

    links.append(award_link)

    return {"nodes": nodes, "links": links}
