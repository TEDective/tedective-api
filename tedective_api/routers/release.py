# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import uuid
from datetime import datetime
from typing import List

from fastapi import APIRouter, Depends, HTTPException, Query
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport
from gql.transport.exceptions import TransportProtocolError, TransportServerError

router = APIRouter()


@router.get(
    "/entities/release/{ocid}",
    tags=["releases"],
    description="Read release by OCID",
)
async def read_release(ocid: str):
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]

    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})
    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:
        query = gql(
            """
            query getRelease($ocid: String!) {
                Release(filter: { ocid: {eq: $ocid} }) {
                    ocid
                    id
                    date
                    tag
                    initiationType
                    parties {
                        id
                        name
                        identifier {
                            scheme
                            legalName
                            uri
                        }
                        additionalIdentifiers {
                            scheme
                            legalName
                            uri
                        }
                        address {
                            countryCode
                            locality
                            postalCode
                            region
                            streetAddress 
                        }
                        contactPoint {
                            name
                            email
                            telephone
                            faxNumber
                            url
                        }
                        roles
                        details
                    }
                    buyer {
                        id
                        name
                        identifier {
                            scheme
                            legalName
                            uri
                        }
                        additionalIdentifiers {
                            scheme
                            legalName
                            uri
                        }
                        address {
                            countryCode
                            locality
                            postalCode
                            region
                            streetAddress 
                        }
                        contactPoint {
                            name
                            email
                            telephone
                            faxNumber
                            url
                        }
                        roles
                        details
                    }
                    planning {
                        rationale
                        budget {
                            description
                            amount {
                                amount
                                currency
                            }
                            project
                            projectID
                            uri
                            source
                        }
                        documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                        }
                        milestones {
                            title
                            type
                            description
                            code
                            dueDate
                            dateMet
                            dateModified
                            status
                            documents {
                                documentType
                                title
                                description
                                url
                                datePublished
                                dateModified
                                format
                                language
                            }
                        }
                    }
                    tender {
                        id
                        title
                        description
                        status
                        procuringEntity {
                            id
                            name
                            identifier {
                                scheme
                                legalName
                                uri
                            }
                            additionalIdentifiers {
                                scheme
                                legalName
                                uri
                            }
                            address {
                                countryCode
                                locality
                                postalCode
                                region
                                streetAddress 
                            }
                            contactPoint {
                                name
                                email
                                telephone
                                faxNumber
                                url
                            }
                            roles
                            details
                        }
                        items {
                            description
                            classification {
                                scheme
                                description
                                uri
                            }
                            additionalClassifications {
                                scheme
                                description
                                uri
                            }
                            quantity
                            unit {
                                scheme
                                name
                                value {
                                    amount
                                    currency
                                }
                                uri
                            }
                        }
                        value {
                            amount
                            currency
                        }
                        minValue {
                            amount
                            currency
                        }
                        procurementMethod
                        procurementMethodDetails
                        procurementMethodRationale
                        mainProcurementCategory
                        additionalProcurementCategories
                        submissionMethod
                        submissionMethodDetails
                        tenderPeriod {
                            startDate
                            endDate
                            maxExtentDate
                            durationInDays
                        }
                        enquiryPeriod {
                            startDate
                            endDate
                            maxExtentDate
                            durationInDays
                        }
                        hasEnquiries
                        eligibilityCriteria
                        awardPeriod {
                            startDate
                            endDate
                            maxExtentDate
                            durationInDays
                        }
                        contractPeriod {
                            startDate
                            endDate
                            maxExtentDate
                            durationInDays
                        }
                        numberOfTenderers
                        tenderers {
                            name
                            identifier {
                                scheme
                                legalName
                                uri
                            }
                            additionalIdentifiers {
                                scheme
                                legalName
                                uri
                            }
                            address {
                                countryCode
                                locality
                                postalCode
                                region
                                streetAddress 
                            }
                            contactPoint {
                                name
                                email
                                telephone
                                faxNumber
                                url
                            }
                            roles
                            details
                        }
                        documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                        }
                        milestones {
                            title
                            type
                            description
                            code
                            dueDate
                            dateMet
                            dateModified
                            status
                            documents {
                                documentType
                                title
                                description
                                url
                                datePublished
                                dateModified
                                format
                                language
                            }
                        }
                        amendments {
                            date
                            rationale
                            description
                            amendsReleaseID
                            releaseID
                            changes {
                                property
                                former_value
                            }
                        }
                        amendment {
                            date
                            rationale
                            description
                            amendsReleaseID
                            releaseID
                            changes {
                                property
                                former_value
                            }
                        }
                        crossBorderLaw
                    }
                		awards {
                    		title
                    		description
                    		status
                    		date
                    		value {
                        	amount
                        	currency
                    		}
                    		suppliers {
                        	name
                        	identifier {
                            	scheme
                            	legalName
                              uri
                        	}
                        	additionalIdentifiers {
                            	scheme
                            	legalName
                            	uri
                          }
                        	address {
                            	countryCode
                                locality
                                postalCode
                                region
                                streetAddress 
                        	}
                        	contactPoint {
                            	name
                            	email
                            	telephone
                            	faxNumber
                            	url
                        	}
                        	roles
                        	details
                    		}
                    		items {
                        		description
                        		classification {
                            	scheme
                            	description
                        		}
                        		additionalClassifications {
                            	scheme
                            	description
                            	uri
                        		}
                        		quantity
                        		unit {
                            	scheme
                            	name
                            	value {
                                	amount
                                	currency
                            	}
                            	uri
                        		}
                    		}
                    		contractPeriod {
                        	startDate
                        	endDate
                        	maxExtentDate
                        	durationInDays
                    		}
                    		documents {
                        	documentType
                        	title
                        	description
                        	url
                        	datePublished
                        	dateModified
                        	format
                        	language
                    		}
                    		amendments {
                        	date
                        	rationale
                        	description
                        	amendsReleaseID
                        	releaseID
                        	changes {
                            	property
                            	former_value
                        	}
                    		}
                    		amendment {
                        	date
                        	rationale
                        	description
                        	amendsReleaseID
                        	releaseID
                        	changes {
                            	property
                            	former_value
                        	}
                    		}
                		}
                		contracts {
                    awardID
                    title
                    description
                    status
                    period {
                        startDate
                        endDate
                        maxExtentDate
                        durationInDays
                    }
                    value {
                        amount
                        currency
                    }
                    items {
                        description
                        classification {
                            scheme
                            description
                            uri
                        }
                        additionalClassifications {
                            scheme
                            description
                            uri
                        }
                        quantity
                        unit {
                            scheme
                            name
                            value {
                                amount
                                currency
                            }
                            uri
                        }
                    }
                    dateSigned
                    documents {
                        documentType
                        title
                        description
                        url
                        datePublished
                        dateModified
                        format
                        language
                    }
                    implementation {
                        transactions {
                            source
                            date
                            value {
                                amount
                                currency
                            }
                            payer {
                                name
                                identifier {
                                    scheme
                                    legalName
                                    uri
                                }
                                additionalIdentifiers {
                                    scheme
                                    legalName
                                    uri
                                }
                                address {
                                    countryCode
                                    locality
                                    postalCode
                                    region
                                    streetAddress 
                                }
                                contactPoint {
                                    name
                                    email
                                    telephone
                                    faxNumber
                                    url
                                }
                                roles
                                details
                            }
                            payee {
                                name
                                identifier {
                                    scheme
                                    legalName
                                    uri
                                }
                                additionalIdentifiers {
                                    scheme
                                    legalName
                                    uri
                                }
                                address {
                                    countryCode
                                    locality
                                    postalCode
                                    region
                                    streetAddress    
                                } 
                                contactPoint {
                                    name
                                    email
                                    telephone
                                    faxNumber
                                    url
                                }
                                roles
                                details
                            }
                            uri
                            amount {
                                amount
                                currency
                            }
                            providerOrganization {
                                scheme
                                legalName
                                uri
                            }
                            receiverOrganization {
                                scheme
                                legalName
                                uri
                            }
                        }
                        milestones {
                            title
                            type
                            description
                            code
                            dueDate
                            dateMet
                            dateModified
                            status
                            documents {
                                documentType
                                title
                                description
                                url
                                datePublished
                                dateModified
                                format
                                language
                            }
                        }
                        documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                        }
                    }
                    relatedProcesses {
                        relationship
                        title
                        scheme
                        identifier
                        uri
                    }
                    milestones {
                        title
                        type
                        description
                        code
                        dueDate
                        dateMet
                        dateModified
                        status
                        documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                        }
                    }
                    amendments {
                        date
                        rationale
                        description
                        amendsReleaseID
                        releaseID
                        changes {
                            property
                            former_value
                        }
                    }
                    amendment {
                        date
                        rationale
                        description
                        amendsReleaseID
                        releaseID
                        changes {
                            property
                            former_value
                        }
                    }
            		relatedProcesses {
            		    relationship
        				title
        				scheme
                		identifier
        				uri
    				}
                }
                language
                relatedProcesses {
                    relationship
                    title
                    scheme
                    identifier
                    uri
                }
            }
        }
        """
        )
        params = {"ocid": ocid}
        try:
            result = await client.execute(query, variable_values=params)
            return result["Release"][0]
        except IndexError or TransportProtocolError or TransportServerError:
            raise HTTPException(status_code=404, detail="Release not found")


# @router.get(
#     "/releases",
#     #response_model=List[ReleaseRead],
#     tags=["releases"],
#     description="Read releases",
# )
# async def read_releases(
#     orgid: uuid.UUID = None,
#     is_buyer: bool = True,
#     start_date: datetime = None,
#     end_date: datetime = None,
#     #limit: int = Query(default=25, lte=100),
# ):
#     """
#     Return releases
#
#     :param session:
#     :param orgid:
#     :param is_buyer:
#     :param start_date:
#     :param end_date:
#     :param offset:
#     :param limit:
#     :return:
#     """
#     address = os.environ["ADDRESS_GQL"]
#     headers = os.environ["HEADERS"]
#
#     transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})
#     async with Client(
#             transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
#     ) as client:
#         query = gql(
#             """
#             query getRelease($orgid: String!) {
#                 Release(filter: { ocid: {eq: $ocid} }) {
#                     ocid
#                     date
#                     tag
#                     initiationType
#                     parties {
#                         name
#                         identifier {
#                             scheme
#                             legalName
#                             uri
#                         }
#                         additionalIdentifiers {
#                             scheme
#                             legalName
#                             uri
#                         }
#                         address {
#                             country {
#                                 name
#                                 also_know_as
#                             }
#                             postal_code
#                             street
#                         }
#                         contactPoint {
#                             name
#                             email
#                             telephone
#                             faxNumber
#                             url
#                         }
#                         roles
#                         details
#                     }
#                     buyer {
#                         name
#                         identifier {
#                             scheme
#                             legalName
#                             uri
#                         }
#                         additionalIdentifiers {
#                             scheme
#                             legalName
#                             uri
#                         }
#                         address {
#                             country {
#                                 name
#                                 also_know_as
#                             }
#                             postal_code
#                             street
#                         }
#                         contactPoint {
#                             name
#                             email
#                             telephone
#                             faxNumber
#                             url
#                         }
#                         roles
#                         details
#                     }
#                     planning {
#                         rationale
#                         budget {
#                             description
#                             amount {
#                                 amount
#                                 currency
#                             }
#                             project
#                             projectID
#                             uri
#                             source
#                         }
#                         documents {
#                             documentType
#                             title
#                             description
#                             url
#                             datePublished
#                             dateModified
#                             format
#                             language
#                         }
#                         milestones {
#                             title
#                             type
#                             description
#                             code
#                             dueDate
#                             dateMet
#                             dateModified
#                             status
#                             documents {
#                                 documentType
#                                 title
#                                 description
#                                 url
#                                 datePublished
#                                 dateModified
#                                 format
#                                 language
#                             }
#                         }
#                     }
#                     tender {
#                         title
#                         description
#                         status
#                         procuringEntity {
#                             name
#                             identifier {
#                                 scheme
#                                 legalName
#                                 uri
#                             }
#                             additionalIdentifiers {
#                                 scheme
#                                 legalName
#                                 uri
#                             }
#                             address {
#                                 country {
#                                     name
#                                     also_know_as
#                                 }
#                                 postal_code
#                                 street
#                             }
#                             contactPoint {
#                                 name
#                                 email
#                                 telephone
#                                 faxNumber
#                                 url
#                             }
#                             roles
#                             details
#                         }
#                         items {
#                             description
#                             classification {
#                                 scheme
#                                 description
#                                 uri
#                             }
#                             additionalClassifications {
#                                 scheme
#                                 description
#                                 uri
#                             }
#                             quantity
#                             unit {
#                                 scheme
#                                 name
#                                 value {
#                                     amount
#                                     currency
#                                 }
#                                 uri
#                             }
#                         }
#                         value {
#                             amount
#                             currency
#                         }
#                         minValue {
#                             amount
#                             currency
#                         }
#                         procurementMethod
#                         procurementMethodDetails
#                         procurementMethodRationale
#                         mainProcurementCategory
#                         additionalProcurementCategories
#                         submissionMethod
#                         submissionMethodDetails
#                         tenderPeriod {
#                             startDate
#                             endDate
#                             maxExtentDate
#                             durationInDays
#                         }
#                         enquiryPeriod {
#                             startDate
#                             endDate
#                             maxExtentDate
#                             durationInDays
#                         }
#                         hasEnquiries
#                         eligibilityCriteria
#                         awardPeriod {
#                             startDate
#                             endDate
#                             maxExtentDate
#                             durationInDays
#                         }
#                         contractPeriod {
#                             startDate
#                             endDate
#                             maxExtentDate
#                             durationInDays
#                         }
#                         numberOfTenderers
#                         tenderers {
#                             name
#                             identifier {
#                                 scheme
#                                 legalName
#                                 uri
#                             }
#                             additionalIdentifiers {
#                                 scheme
#                                 legalName
#                                 uri
#                             }
#                             address {
#                                 country {
#                                     name
#                                     also_know_as
#                                 }
#                                 postal_code
#                                 street
#                             }
#                             contactPoint {
#                                 name
#                                 email
#                                 telephone
#                                 faxNumber
#                                 url
#                             }
#                             roles
#                             details
#                         }
#                         documents {
#                             documentType
#                             title
#                             description
#                             url
#                             datePublished
#                             dateModified
#                             format
#                             language
#                         }
#                         milestones {
#                             title
#                             type
#                             description
#                             code
#                             dueDate
#                             dateMet
#                             dateModified
#                             status
#                             documents {
#                                 documentType
#                                 title
#                                 description
#                                 url
#                                 datePublished
#                                 dateModified
#                                 format
#                                 language
#                             }
#                         }
#                         amendments {
#                             date
#                             rationale
#                             description
#                             amendsReleaseID
#                             releaseID
#                             changes {
#                                 property
#                                 former_value
#                             }
#                         }
#                         amendment {
#                             date
#                             rationale
#                             description
#                             amendsReleaseID
#                             releaseID
#                             changes {
#                                 property
#                                 former_value
#                             }
#                         }
#                         crossBorderLaw
#                     }
#                         awards {
#                             title
#                             description
#                             status
#                             date
#                             value {
#                             amount
#                             currency
#                             }
#                             suppliers {
#                             name
#                             identifier {
#                                 scheme
#                                 legalName
#                               uri
#                             }
#                             additionalIdentifiers {
#                                 scheme
#                                 legalName
#                                 uri
#                           }
#                             address {
#                                 country {
#                                     name
#                                     also_know_as
#                                 }
#                                 postal_code
#                                 street
#                             }
#                             contactPoint {
#                                 name
#                                 email
#                                 telephone
#                                 faxNumber
#                                 url
#                             }
#                             roles
#                             details
#                             }
#                             items {
#                                 description
#                                 classification {
#                                 scheme
#                                 description
#                                 }
#                                 additionalClassifications {
#                                 scheme
#                                 description
#                                 uri
#                                 }
#                                 quantity
#                                 unit {
#                                 scheme
#                                 name
#                                 value {
#                                     amount
#                                     currency
#                                 }
#                                 uri
#                                 }
#                             }
#                             contractPeriod {
#                             startDate
#                             endDate
#                             maxExtentDate
#                             durationInDays
#                             }
#                             documents {
#                             documentType
#                             title
#                             description
#                             url
#                             datePublished
#                             dateModified
#                             format
#                             language
#                             }
#                             amendments {
#                             date
#                             rationale
#                             description
#                             amendsReleaseID
#                             releaseID
#                             changes {
#                                 property
#                                 former_value
#                             }
#                             }
#                             amendment {
#                             date
#                             rationale
#                             description
#                             amendsReleaseID
#                             releaseID
#                             changes {
#                                 property
#                                 former_value
#                             }
#                             }
#                         }
#                         contracts {
#                     awardID
#                     title
#                     description
#                     status
#                     period {
#                         startDate
#                         endDate
#                         maxExtentDate
#                         durationInDays
#                     }
#                     value {
#                         amount
#                         currency
#                     }
#                     items {
#                         description
#                         classification {
#                             scheme
#                             description
#                             uri
#                         }
#                         additionalClassifications {
#                             scheme
#                             description
#                             uri
#                         }
#                         quantity
#                         unit {
#                             scheme
#                             name
#                             value {
#                                 amount
#                                 currency
#                             }
#                             uri
#                         }
#                     }
#                     dateSigned
#                     documents {
#                         documentType
#                         title
#                         description
#                         url
#                         datePublished
#                         dateModified
#                         format
#                         language
#                     }
#                     implementation {
#                         transactions {
#                             source
#                             date
#                             value {
#                                 amount
#                                 currency
#                             }
#                             payer {
#                                 name
#                                 identifier {
#                                     scheme
#                                     legalName
#                                     uri
#                                 }
#                                 additionalIdentifiers {
#                                     scheme
#                                     legalName
#                                     uri
#                                 }
#                                 address {
#                                     country {
#                                         name
#                                         also_know_as
#                                     }
#                                     postal_code
#                                     street
#                                 }
#                                 contactPoint {
#                                     name
#                                     email
#                                     telephone
#                                     faxNumber
#                                     url
#                                 }
#                                 roles
#                                 details
#                             }
#                             payee {
#                                 name
#                                 identifier {
#                                     scheme
#                                     legalName
#                                     uri
#                                 }
#                                 additionalIdentifiers {
#                                     scheme
#                                     legalName
#                                     uri
#                                 }
#                                 address {
#                                     country {
#                                         name
#                                         also_know_as
#                                     }
#                                     postal_code
#                                     street
#                                 }
#                                 contactPoint {
#                                     name
#                                     email
#                                     telephone
#                                     faxNumber
#                                     url
#                                 }
#                                 roles
#                                 details
#                             }
#                             uri
#                             amount {
#                                 amount
#                                 currency
#                             }
#                             providerOrganization {
#                                 scheme
#                                 legalName
#                                 uri
#                             }
#                             receiverOrganization {
#                                 scheme
#                                 legalName
#                                 uri
#                             }
#                         }
#                         milestones {
#                             title
#                             type
#                             description
#                             code
#                             dueDate
#                             dateMet
#                             dateModified
#                             status
#                             documents {
#                                 documentType
#                                 title
#                                 description
#                                 url
#                                 datePublished
#                                 dateModified
#                                 format
#                                 language
#                             }
#                         }
#                         documents {
#                             documentType
#                             title
#                             description
#                             url
#                             datePublished
#                             dateModified
#                             format
#                             language
#                         }
#                     }
#                     relatedProcesses {
#                         relationship
#                         title
#                         scheme
#                         identifier
#                         uri
#                     }
#                     milestones {
#                         title
#                         type
#                         description
#                         code
#                         dueDate
#                         dateMet
#                         dateModified
#                         status
#                         documents {
#                             documentType
#                             title
#                             description
#                             url
#                             datePublished
#                             dateModified
#                             format
#                             language
#                         }
#                     }
#                     amendments {
#                         date
#                         rationale
#                         description
#                         amendsReleaseID
#                         releaseID
#                         changes {
#                             property
#                             former_value
#                         }
#                     }
#                     amendment {
#                         date
#                         rationale
#                         description
#                         amendsReleaseID
#                         releaseID
#                         changes {
#                             property
#                             former_value
#                         }
#                     }
#                     relatedProcesses {
#                         relationship
#                         title
#                         scheme
#                         identifier
#                         uri
#                     }
#                 }
#             }
#         }
#         """
#         )
#         params = {"ocid": ocid}
#         try:
#             result = await client.execute(query, variable_values=params)
#             return result
#         except TransportProtocolError or TransportServerError or IndexError:
#             raise HTTPException(status_code=404, detail="Release not found")


@router.get(
    "/entities/releases/{ocid}",
    # response_model=List[ReleaseRead],
    tags=["releases"],
    description="Read releases for OCID",
)
async def read_releases_ocid(
    ocid: str,
    # limit: int = Query(default=25, lte=100),
):
    """
    Return releases for OCID

    :param ocid:
    :param session:
    :param offset:
    :param limit:
    :return:
    """
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]

    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})
    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:
        query = gql(
            """
            query getRelease($ocid: String!) {
                Release(filter: { ocid: {eq: $ocid} }) {
                    ocid
                    date
                    tag
                    initiationType
                    parties {
                        name
                        identifier {
                            scheme
                            legalName
                            uri
                        }
                        additionalIdentifiers {
                            scheme
                            legalName
                            uri
                        }
                        address {
                            countryCode
                            locality
                            postalCode
                            region
                            streetAddress 
                        }
                        contactPoint {
                            name
                            email
                            telephone
                            faxNumber
                            url
                        }
                        roles
                        details
                    }
                    buyer {
                        name
                        identifier {
                            scheme
                            legalName
                            uri
                        }
                        additionalIdentifiers {
                            scheme
                            legalName
                            uri
                        }
                        address {
                            countryCode
                            locality
                            postalCode
                            region
                            streetAddress 
                        }
                        contactPoint {
                            name
                            email
                            telephone
                            faxNumber
                            url
                        }
                        roles
                        details
                    }
                    planning {
                        rationale
                        budget {
                            description
                            amount {
                                amount
                                currency
                            }
                            project
                            projectID
                            uri
                            source
                        }
                        documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                        }
                        milestones {
                            title
                            type
                            description
                            code
                            dueDate
                            dateMet
                            dateModified
                            status
                            documents {
                                documentType
                                title
                                description
                                url
                                datePublished
                                dateModified
                                format
                                language
                            }
                        }
                    }
                    tender {
                        title
                        description
                        status
                        procuringEntity {
                            name
                            identifier {
                                scheme
                                legalName
                                uri
                            }
                            additionalIdentifiers {
                                scheme
                                legalName
                                uri
                            }
                            address {
                                countryCode
                                locality
                                postalCode
                                region
                                streetAddress
                            }
                            contactPoint {
                                name
                                email
                                telephone
                                faxNumber
                                url
                            }
                            roles
                            details
                        }
                        items {
                            description
                            classification {
                                scheme
                                description
                                uri
                            }
                            additionalClassifications {
                                scheme
                                description
                                uri
                            }
                            quantity
                            unit {
                                scheme
                                name
                                value {
                                    amount
                                    currency
                                }
                                uri
                            }
                        }
                        value {
                            amount
                            currency
                        }
                        minValue {
                            amount
                            currency
                        }
                        procurementMethod
                        procurementMethodDetails
                        procurementMethodRationale
                        mainProcurementCategory
                        additionalProcurementCategories
                        submissionMethod
                        submissionMethodDetails
                        tenderPeriod {
                            startDate
                            endDate
                            maxExtentDate
                            durationInDays
                        }
                        enquiryPeriod {
                            startDate
                            endDate
                            maxExtentDate
                            durationInDays
                        }
                        hasEnquiries
                        eligibilityCriteria
                        awardPeriod {
                            startDate
                            endDate
                            maxExtentDate
                            durationInDays
                        }
                        contractPeriod {
                            startDate
                            endDate
                            maxExtentDate
                            durationInDays
                        }
                        numberOfTenderers
                        tenderers {
                            name
                            identifier {
                                scheme
                                legalName
                                uri
                            }
                            additionalIdentifiers {
                                scheme
                                legalName
                                uri
                            }
                            address {
                                countryCode
                                locality
                                postalCode
                                region
                                streetAddress 
                            }
                            contactPoint {
                                name
                                email
                                telephone
                                faxNumber
                                url
                            }
                            roles
                            details
                        }
                        documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                        }
                        milestones {
                            title
                            type
                            description
                            code
                            dueDate
                            dateMet
                            dateModified
                            status
                            documents {
                                documentType
                                title
                                description
                                url
                                datePublished
                                dateModified
                                format
                                language
                            }
                        }
                        amendments {
                            date
                            rationale
                            description
                            amendsReleaseID
                            releaseID
                            changes {
                                property
                                former_value
                            }
                        }
                        amendment {
                            date
                            rationale
                            description
                            amendsReleaseID
                            releaseID
                            changes {
                                property
                                former_value
                            }
                        }
                        crossBorderLaw
                    }
                        awards {
                            title
                            description
                            status
                            date
                            value {
                            amount
                            currency
                            }
                            suppliers {
                            name
                            identifier {
                                scheme
                                legalName
                              uri
                            }
                            additionalIdentifiers {
                                scheme
                                legalName
                                uri
                          }
                            address {
                                countryCode
                                locality
                                postalCode
                                region
                                streetAddress
                            }
                            contactPoint {
                                name
                                email
                                telephone
                                faxNumber
                                url
                            }
                            roles
                            details
                            }
                            items {
                                description
                                classification {
                                scheme
                                description
                                }
                                additionalClassifications {
                                scheme
                                description
                                uri
                                }
                                quantity
                                unit {
                                scheme
                                name
                                value {
                                    amount
                                    currency
                                }
                                uri
                                }
                            }
                            contractPeriod {
                            startDate
                            endDate
                            maxExtentDate
                            durationInDays
                            }
                            documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                            }
                            amendments {
                            date
                            rationale
                            description
                            amendsReleaseID
                            releaseID
                            changes {
                                property
                                former_value
                            }
                            }
                            amendment {
                            date
                            rationale
                            description
                            amendsReleaseID
                            releaseID
                            changes {
                                property
                                former_value
                            }
                            }
                        }
                        contracts {
                    awardID
                    title
                    description
                    status
                    period {
                        startDate
                        endDate
                        maxExtentDate
                        durationInDays
                    }
                    value {
                        amount
                        currency
                    }
                    items {
                        description
                        classification {
                            scheme
                            description
                            uri
                        }
                        additionalClassifications {
                            scheme
                            description
                            uri
                        }
                        quantity
                        unit {
                            scheme
                            name
                            value {
                                amount
                                currency
                            }
                            uri
                        }
                    }
                    dateSigned
                    documents {
                        documentType
                        title
                        description
                        url
                        datePublished
                        dateModified
                        format
                        language
                    }
                    implementation {
                        transactions {
                            source
                            date
                            value {
                                amount
                                currency
                            }
                            payer {
                                name
                                identifier {
                                    scheme
                                    legalName
                                    uri
                                }
                                additionalIdentifiers {
                                    scheme
                                    legalName
                                    uri
                                }
                                address {
                                    countryCode
                                    locality
                                    postalCode
                                    region
                                    streetAddress
                                }
                                contactPoint {
                                    name
                                    email
                                    telephone
                                    faxNumber
                                    url
                                }
                                roles
                                details
                            }
                            payee {
                                name
                                identifier {
                                    scheme
                                    legalName
                                    uri
                                }
                                additionalIdentifiers {
                                    scheme
                                    legalName
                                    uri
                                }
                                address {
                                    countryCode
                                    locality
                                    postalCode
                                    region
                                    streetAddress
                                }
                                contactPoint {
                                    name
                                    email
                                    telephone
                                    faxNumber
                                    url
                                }
                                roles
                                details
                            }
                            uri
                            amount {
                                amount
                                currency
                            }
                            providerOrganization {
                                scheme
                                legalName
                                uri
                            }
                            receiverOrganization {
                                scheme
                                legalName
                                uri
                            }
                        }
                        milestones {
                            title
                            type
                            description
                            code
                            dueDate
                            dateMet
                            dateModified
                            status
                            documents {
                                documentType
                                title
                                description
                                url
                                datePublished
                                dateModified
                                format
                                language
                            }
                        }
                        documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                        }
                    }
                    relatedProcesses {
                        relationship
                        title
                        scheme
                        identifier
                        uri
                    }
                    milestones {
                        title
                        type
                        description
                        code
                        dueDate
                        dateMet
                        dateModified
                        status
                        documents {
                            documentType
                            title
                            description
                            url
                            datePublished
                            dateModified
                            format
                            language
                        }
                    }
                    amendments {
                        date
                        rationale
                        description
                        amendsReleaseID
                        releaseID
                        changes {
                            property
                            former_value
                        }
                    }
                    amendment {
                        date
                        rationale
                        description
                        amendsReleaseID
                        releaseID
                        changes {
                            property
                            former_value
                        }
                    }
                    relatedProcesses {
                        relationship
                        title
                        scheme
                        identifier
                        uri
                    }
                }
                language
                relatedProcesses {
                    relationship
                    title
                    scheme
                    identifier
                    uri
                }
            }
        }
        """
        )
        params = {"ocid": ocid}
        try:
            result = await client.execute(query, variable_values=params)
            return result
        except TransportProtocolError or TransportServerError or IndexError:
            raise HTTPException(status_code=404, detail="Release not found")


# Releases per organizations (buyer/supplier)
@router.get("/entities/releases/buyer/{id}", tags=["releases"])
async def read_releases_per_buyer(id: uuid.UUID):
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]
    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})

    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:
        query = gql(
            """
                        query getRelease($id: String!) {
                            Release(filter: {buyer: {someHave: {id: {eq: $id}}}}) {
                                ocid
                                date
                                tag
                                initiationType
                                parties {
                                    id
                                    name
                                    identifier {
                                        scheme
                                        legalName
                                        uri
                                    }
                                    additionalIdentifiers {
                                        scheme
                                        legalName
                                        uri
                                    }
                                    address {
                                        countryCode
                                        locality
                                        postalCode
                                        region
                                        streetAddress
                                    }
                                    contactPoint {
                                        name
                                        email
                                        telephone
                                        faxNumber
                                        url
                                    }
                                    roles
                                    details
                                }
                                buyer {
                                    id
                                    name
                                    identifier {
                                        scheme
                                        legalName
                                        uri
                                    }
                                    additionalIdentifiers {
                                        scheme
                                        legalName
                                        uri
                                    }
                                    address {
                                        countryCode
                                        locality
                                        postalCode
                                        region
                                        streetAddress
                                    }
                                    contactPoint {
                                        name
                                        email
                                        telephone
                                        faxNumber
                                        url
                                    }
                                    roles
                                    details
                                }
                                planning {
                                    rationale
                                    budget {
                                        description
                                        amount {
                                            amount
                                            currency
                                        }
                                        project
                                        projectID
                                        uri
                                        source
                                    }
                                    documents {
                                        documentType
                                        title
                                        description
                                        url
                                        datePublished
                                        dateModified
                                        format
                                        language
                                    }
                                    milestones {
                                        title
                                        type
                                        description
                                        code
                                        dueDate
                                        dateMet
                                        dateModified
                                        status
                                        documents {
                                            documentType
                                            title
                                            description
                                            url
                                            datePublished
                                            dateModified
                                            format
                                            language
                                        }
                                    }
                                }
                                tender {
                                    id
                                    title
                                    description
                                    status
                                    procuringEntity {
                                        id
                                        name
                                        identifier {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        additionalIdentifiers {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        address {
                                            countryCode
                                            locality
                                            postalCode
                                            region
                                            streetAddress
                                        }
                                        contactPoint {
                                            name
                                            email
                                            telephone
                                            faxNumber
                                            url
                                        }
                                        roles
                                        details
                                    }
                                    items {
                                        description
                                        classification {
                                            scheme
                                            description
                                            uri
                                        }
                                        additionalClassifications {
                                            scheme
                                            description
                                            uri
                                        }
                                        quantity
                                        unit {
                                            scheme
                                            name
                                            value {
                                                amount
                                                currency
                                            }
                                            uri
                                        }
                                    }
                                    value {
                                        amount
                                        currency
                                    }
                                    minValue {
                                        amount
                                        currency
                                    }
                                    procurementMethod
                                    procurementMethodDetails
                                    procurementMethodRationale
                                    mainProcurementCategory
                                    additionalProcurementCategories
                                    awardCriteria
                                    awardCriteriaDetails
                                    submissionMethod
                                    submissionMethodDetails
                                    tenderPeriod {
                                        startDate
                                        endDate
                                        maxExtentDate
                                        durationInDays
                                    }
                                    enquiryPeriod {
                                        startDate
                                        endDate
                                        maxExtentDate
                                        durationInDays
                                    }
                                    hasEnquiries
                                    eligibilityCriteria
                                    awardPeriod {
                                        startDate
                                        endDate
                                        maxExtentDate
                                        durationInDays
                                    }
                                    contractPeriod {
                                        startDate
                                        endDate
                                        maxExtentDate
                                        durationInDays
                                    }
                                    numberOfTenderers
                                    tenderers {
                                        id
                                        name
                                        identifier {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        additionalIdentifiers {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        address {
                                            countryCode
                                            locality
                                            postalCode
                                            region
                                            streetAddress
                                        }
                                        contactPoint {
                                            name
                                            email
                                            telephone
                                            faxNumber
                                            url
                                        }
                                        roles
                                        details
                                    }
                                    documents {
                                        documentType
                                        title
                                        description
                                        url
                                        datePublished
                                        dateModified
                                        format
                                        language
                                    }
                                    milestones {
                                        title
                                        type
                                        description
                                        code
                                        dueDate
                                        dateMet
                                        dateModified
                                        status
                                        documents {
                                            documentType
                                            title
                                            description
                                            url
                                            datePublished
                                            dateModified
                                            format
                                            language
                                        }
                                    }
                                    amendments {
                                        date
                                        rationale
                                        description
                                        amendsReleaseID
                                        releaseID
                                        changes {
                                            property
                                            former_value
                                        }
                                    }
                                    amendment {
                                        date
                                        rationale
                                        description
                                        amendsReleaseID
                                        releaseID
                                        changes {
                                            property
                                            former_value
                                        }
                                    }
                                    crossBorderLaw
                                }
                                    awards {
                                        id
                                        title
                                        description
                                        status
                                        date
                                        value {
                                        amount
                                        currency
                                        }
                                        suppliers {
                                        id
                                        name
                                        identifier {
                                            scheme
                                            legalName
                                          uri
                                        }
                                        additionalIdentifiers {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        address {
                                            countryCode
                                            locality
                                            postalCode
                                            region
                                            streetAddress
                                        }
                                        contactPoint {
                                            name
                                            email
                                            telephone
                                            faxNumber
                                            url
                                        }
                                        roles
                                        details
                                        }
                                        items {
                                            description
                                            classification {
                                            scheme
                                            description
                                            }
                                            additionalClassifications {
                                            scheme
                                            description
                                            uri
                                            }
                                            quantity
                                            unit {
                                            scheme
                                            name
                                            value {
                                                amount
                                                currency
                                            }
                                            uri
                                            }
                                        }
                                        contractPeriod {
                                        startDate
                                        endDate
                                        maxExtentDate
                                        durationInDays
                                        }
                                        documents {
                                        documentType
                                        title
                                        description
                                        url
                                        datePublished
                                        dateModified
                                        format
                                        language
                                        }
                                        amendments {
                                        date
                                        rationale
                                        description
                                        amendsReleaseID
                                        releaseID
                                        changes {
                                            property
                                            former_value
                                        }
                                        }
                                        amendment {
                                        date
                                        rationale
                                        description
                                        amendsReleaseID
                                        releaseID
                                        changes {
                                            property
                                            former_value
                                        }
                                        }
                                    }
                                    contracts {
                                id
                                awardID
                                title
                                description
                                status
                                period {
                                    startDate
                                    endDate
                                    maxExtentDate
                                    durationInDays
                                }
                                value {
                                    amount
                                    currency
                                }
                                items {
                                    description
                                    classification {
                                        scheme
                                        description
                                        uri
                                    }
                                    additionalClassifications {
                                        scheme
                                        description
                                        uri
                                    }
                                    quantity
                                    unit {
                                        scheme
                                        name
                                        value {
                                            amount
                                            currency
                                        }
                                        uri
                                    }
                                }
                                dateSigned
                                documents {
                                    documentType
                                    title
                                    description
                                    url
                                    datePublished
                                    dateModified
                                    format
                                    language
                                }
                                implementation {
                                    transactions {
                                        source
                                        date
                                        value {
                                            amount
                                            currency
                                        }
                                        payer {
                                            id
                                            name
                                            identifier {
                                                scheme
                                                legalName
                                                uri
                                            }
                                            additionalIdentifiers {
                                                scheme
                                                legalName
                                                uri
                                            }
                                            address {
                                                countryCode
                                                locality
                                                postalCode
                                                region
                                                streetAddress
                                            }
                                            contactPoint {
                                                name
                                                email
                                                telephone
                                                faxNumber
                                                url
                                            }
                                            roles
                                            details
                                        }
                                        payee {
                                            id
                                            name
                                            identifier {
                                                scheme
                                                legalName
                                                uri
                                            }
                                            additionalIdentifiers {
                                                scheme
                                                legalName
                                                uri
                                            }
                                            address {
                                                countryCode
                                                locality
                                                postalCode
                                                region
                                                streetAddress
                                            }
                                            contactPoint {
                                                name
                                                email
                                                telephone
                                                faxNumber
                                                url
                                            }
                                            roles
                                            details
                                        }
                                        uri
                                        amount {
                                            amount
                                            currency
                                        }
                                        providerOrganization {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        receiverOrganization {
                                            scheme
                                            legalName
                                            uri
                                        }
                                    }
                                    milestones {
                                        title
                                        type
                                        description
                                        code
                                        dueDate
                                        dateMet
                                        dateModified
                                        status
                                        documents {
                                            documentType
                                            title
                                            description
                                            url
                                            datePublished
                                            dateModified
                                            format
                                            language
                                        }
                                    }
                                    documents {
                                        documentType
                                        title
                                        description
                                        url
                                        datePublished
                                        dateModified
                                        format
                                        language
                                    }
                                }
                                relatedProcesses {
                                    relationship
                                    title
                                    scheme
                                    identifier
                                    uri
                                }
                                milestones {
                                    title
                                    type
                                    description
                                    code
                                    dueDate
                                    dateMet
                                    dateModified
                                    status
                                    documents {
                                        documentType
                                        title
                                        description
                                        url
                                        datePublished
                                        dateModified
                                        format
                                        language
                                    }
                                }
                                amendments {
                                    date
                                    rationale
                                    description
                                    amendsReleaseID
                                    releaseID
                                    changes {
                                        property
                                        former_value
                                    }
                                }
                                amendment {
                                    date
                                    rationale
                                    description
                                    amendsReleaseID
                                    releaseID
                                    changes {
                                        property
                                        former_value
                                    }
                                }
                                relatedProcesses {
                                    relationship
                                    title
                                    scheme
                                    identifier
                                    uri
                                }
                            }
                            language
                            relatedProcesses {
                                relationship
                                title
                                scheme
                                identifier
                                uri
                            }
                        }
                    }
                    """
        )
        params = {"id": str(id)}
        try:
            result = await client.execute(query, variable_values=params)
            return result["Release"]
        except IndexError or TransportProtocolError or TransportServerError:
            raise HTTPException(status_code=404, detail="Release not found")


# Supplier
@router.get("/entities/releases/supplier/{id}", tags=["releases"])
async def read_releases_per_supplier(id: uuid.UUID):
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]
    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})

    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:
        query = gql(
            """
                        query getRelease($id: String!) {
                            Release(filter: {awards: {someHave: {suppliers: {someHave: {id: {eq: $id}}}}}}) {
                                ocid
                                date
                                tag
                                initiationType
                                parties {
                                    id
                                    name
                                    identifier {
                                        scheme
                                        legalName
                                        uri
                                    }
                                    additionalIdentifiers {
                                        scheme
                                        legalName
                                        uri
                                    }
                                    address {
                                        countryCode
                                        locality
                                        postalCode
                                        region
                                        streetAddress
                                    }
                                    contactPoint {
                                        name
                                        email
                                        telephone
                                        faxNumber
                                        url
                                    }
                                    roles
                                    details
                                }
                                buyer {
                                    id
                                    name
                                    identifier {
                                        scheme
                                        legalName
                                        uri
                                    }
                                    additionalIdentifiers {
                                        scheme
                                        legalName
                                        uri
                                    }
                                    address {
                                        countryCode
                                        locality
                                        postalCode
                                        region
                                        streetAddress
                                    }
                                    contactPoint {
                                        name
                                        email
                                        telephone
                                        faxNumber
                                        url
                                    }
                                    roles
                                    details
                                }
                                planning {
                                    rationale
                                    budget {
                                        description
                                        amount {
                                            amount
                                            currency
                                        }
                                        project
                                        projectID
                                        uri
                                        source
                                    }
                                    documents {
                                        documentType
                                        title
                                        description
                                        url
                                        datePublished
                                        dateModified
                                        format
                                        language
                                    }
                                    milestones {
                                        title
                                        type
                                        description
                                        code
                                        dueDate
                                        dateMet
                                        dateModified
                                        status
                                        documents {
                                            documentType
                                            title
                                            description
                                            url
                                            datePublished
                                            dateModified
                                            format
                                            language
                                        }
                                    }
                                }
                                tender {
                                    id
                                    title
                                    description
                                    status
                                    procuringEntity {
                                        id
                                        name
                                        identifier {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        additionalIdentifiers {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        address {
                                            countryCode
                                            locality
                                            postalCode
                                            region
                                            streetAddress
                                        }
                                        contactPoint {
                                            name
                                            email
                                            telephone
                                            faxNumber
                                            url
                                        }
                                        roles
                                        details
                                    }
                                    items {
                                        description
                                        classification {
                                            scheme
                                            description
                                            uri
                                        }
                                        additionalClassifications {
                                            scheme
                                            description
                                            uri
                                        }
                                        quantity
                                        unit {
                                            scheme
                                            name
                                            value {
                                                amount
                                                currency
                                            }
                                            uri
                                        }
                                    }
                                    value {
                                        amount
                                        currency
                                    }
                                    minValue {
                                        amount
                                        currency
                                    }
                                    procurementMethod
                                    procurementMethodDetails
                                    procurementMethodRationale
                                    mainProcurementCategory
                                    additionalProcurementCategories
                                    awardCriteria
                                    awardCriteriaDetails
                                    submissionMethod
                                    submissionMethodDetails
                                    tenderPeriod {
                                        startDate
                                        endDate
                                        maxExtentDate
                                        durationInDays
                                    }
                                    enquiryPeriod {
                                        startDate
                                        endDate
                                        maxExtentDate
                                        durationInDays
                                    }
                                    hasEnquiries
                                    eligibilityCriteria
                                    awardPeriod {
                                        startDate
                                        endDate
                                        maxExtentDate
                                        durationInDays
                                    }
                                    contractPeriod {
                                        startDate
                                        endDate
                                        maxExtentDate
                                        durationInDays
                                    }
                                    numberOfTenderers
                                    tenderers {
                                        id
                                        name
                                        identifier {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        additionalIdentifiers {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        address {
                                            countryCode
                                            locality
                                            postalCode
                                            region
                                            streetAddress
                                        }
                                        contactPoint {
                                            name
                                            email
                                            telephone
                                            faxNumber
                                            url
                                        }
                                        roles
                                        details
                                    }
                                    documents {
                                        documentType
                                        title
                                        description
                                        url
                                        datePublished
                                        dateModified
                                        format
                                        language
                                    }
                                    milestones {
                                        title
                                        type
                                        description
                                        code
                                        dueDate
                                        dateMet
                                        dateModified
                                        status
                                        documents {
                                            documentType
                                            title
                                            description
                                            url
                                            datePublished
                                            dateModified
                                            format
                                            language
                                        }
                                    }
                                    amendments {
                                        date
                                        rationale
                                        description
                                        amendsReleaseID
                                        releaseID
                                        changes {
                                            property
                                            former_value
                                        }
                                    }
                                    amendment {
                                        date
                                        rationale
                                        description
                                        amendsReleaseID
                                        releaseID
                                        changes {
                                            property
                                            former_value
                                        }
                                    }
                                    crossBorderLaw
                                }
                                    awards {
                                        id
                                        title
                                        description
                                        status
                                        date
                                        value {
                                        amount
                                        currency
                                        }
                                        suppliers {
                                        id
                                        name
                                        identifier {
                                            scheme
                                            legalName
                                          uri
                                        }
                                        additionalIdentifiers {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        address {
                                            countryCode
                                            locality
                                            postalCode
                                            region
                                            streetAddress
                                        }
                                        contactPoint {
                                            name
                                            email
                                            telephone
                                            faxNumber
                                            url
                                        }
                                        roles
                                        details
                                        }
                                        items {
                                            description
                                            classification {
                                            scheme
                                            description
                                            }
                                            additionalClassifications {
                                            scheme
                                            description
                                            uri
                                            }
                                            quantity
                                            unit {
                                            scheme
                                            name
                                            value {
                                                amount
                                                currency
                                            }
                                            uri
                                            }
                                        }
                                        contractPeriod {
                                        startDate
                                        endDate
                                        maxExtentDate
                                        durationInDays
                                        }
                                        documents {
                                        documentType
                                        title
                                        description
                                        url
                                        datePublished
                                        dateModified
                                        format
                                        language
                                        }
                                        amendments {
                                        date
                                        rationale
                                        description
                                        amendsReleaseID
                                        releaseID
                                        changes {
                                            property
                                            former_value
                                        }
                                        }
                                        amendment {
                                        date
                                        rationale
                                        description
                                        amendsReleaseID
                                        releaseID
                                        changes {
                                            property
                                            former_value
                                        }
                                        }
                                    }
                                    contracts {
                                id
                                awardID
                                title
                                description
                                status
                                period {
                                    startDate
                                    endDate
                                    maxExtentDate
                                    durationInDays
                                }
                                value {
                                    amount
                                    currency
                                }
                                items {
                                    description
                                    classification {
                                        scheme
                                        description
                                        uri
                                    }
                                    additionalClassifications {
                                        scheme
                                        description
                                        uri
                                    }
                                    quantity
                                    unit {
                                        scheme
                                        name
                                        value {
                                            amount
                                            currency
                                        }
                                        uri
                                    }
                                }
                                dateSigned
                                documents {
                                    documentType
                                    title
                                    description
                                    url
                                    datePublished
                                    dateModified
                                    format
                                    language
                                }
                                implementation {
                                    transactions {
                                        source
                                        date
                                        value {
                                            amount
                                            currency
                                        }
                                        payer {
                                            name
                                            identifier {
                                                scheme
                                                legalName
                                                uri
                                            }
                                            additionalIdentifiers {
                                                scheme
                                                legalName
                                                uri
                                            }
                                            address {
                                                countryCode
                                                locality
                                                postalCode
                                                region
                                                streetAddress
                                            }
                                            contactPoint {
                                                name
                                                email
                                                telephone
                                                faxNumber
                                                url
                                            }
                                            roles
                                            details
                                        }
                                        payee {
                                            name
                                            identifier {
                                                scheme
                                                legalName
                                                uri
                                            }
                                            additionalIdentifiers {
                                                scheme
                                                legalName
                                                uri
                                            }
                                            address {
                                                countryCode
                                                locality
                                                postalCode
                                                region
                                                streetAddress
                                            }
                                            contactPoint {
                                                name
                                                email
                                                telephone
                                                faxNumber
                                                url
                                            }
                                            roles
                                            details
                                        }
                                        uri
                                        amount {
                                            amount
                                            currency
                                        }
                                        providerOrganization {
                                            scheme
                                            legalName
                                            uri
                                        }
                                        receiverOrganization {
                                            scheme
                                            legalName
                                            uri
                                        }
                                    }
                                    milestones {
                                        title
                                        type
                                        description
                                        code
                                        dueDate
                                        dateMet
                                        dateModified
                                        status
                                        documents {
                                            documentType
                                            title
                                            description
                                            url
                                            datePublished
                                            dateModified
                                            format
                                            language
                                        }
                                    }
                                    documents {
                                        documentType
                                        title
                                        description
                                        url
                                        datePublished
                                        dateModified
                                        format
                                        language
                                    }
                                }
                                relatedProcesses {
                                    relationship
                                    title
                                    scheme
                                    identifier
                                    uri
                                }
                                milestones {
                                    title
                                    type
                                    description
                                    code
                                    dueDate
                                    dateMet
                                    dateModified
                                    status
                                    documents {
                                        documentType
                                        title
                                        description
                                        url
                                        datePublished
                                        dateModified
                                        format
                                        language
                                    }
                                }
                                amendments {
                                    date
                                    rationale
                                    description
                                    amendsReleaseID
                                    releaseID
                                    changes {
                                        property
                                        former_value
                                    }
                                }
                                amendment {
                                    date
                                    rationale
                                    description
                                    amendsReleaseID
                                    releaseID
                                    changes {
                                        property
                                        former_value
                                    }
                                }
                                relatedProcesses {
                                    relationship
                                    title
                                    scheme
                                    identifier
                                    uri
                                }
                            }
                            language
                            relatedProcesses {
                                relationship
                                title
                                scheme
                                identifier
                                uri
                            }
                        }
                    }
                    """
        )
        params = {"id": str(id)}
        try:
            result = await client.execute(query, variable_values=params)
            return result["Release"]
        except IndexError or TransportProtocolError or TransportServerError:
            raise HTTPException(status_code=404, detail="Release not found")
