# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import uuid

from fastapi import APIRouter, Depends, HTTPException
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport
from gql.transport.exceptions import TransportProtocolError, TransportServerError

router = APIRouter()


@router.get("/entities/award/{id}", tags=["awards"])
async def read_award(id: uuid.UUID):
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]
    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})

    # TODO: extended_timetout could be lower, to be changed before releasing to production
    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:
        query = gql(
            """
    query getAward($id: String!) {
        Award(filter: {id: {eq: $id}}) {
            id
            title
            description
            status
            date
            value {
                amount
                currency
            }
            suppliers {
                id
                name
                identifier {
                    scheme
                    legalName
                    uri
                }
                additionalIdentifiers {
                    scheme
                    legalName
                    uri
                }
                address {
                    countryCode
                    locality
                    postalCode
                    region
                    streetAddress
                }
                contactPoint {
                    name
                    email
                    telephone
                    faxNumber
                    url
                }
                roles
                details
            }
            items {
                id
                description
                classification {
                    scheme
                    description
                }
                additionalClassifications {
                    scheme
                    description
                    uri
                }
                quantity
                unit {
                    scheme
                    name
                    value {
                        amount
                        currency
                    }
                    uri
                }
            }
            contractPeriod {
                startDate
                endDate
                maxExtentDate
                durationInDays
            }
            documents {
                documentType
                title
                description
                url
                datePublished
                dateModified
                format
                language
            }
            amendments {
                date
                rationale
                description
                amendsReleaseID
                releaseID
                changes {
                    property
                    former_value
                }
            }
            amendment {
               date
                rationale
                description
                amendsReleaseID
                releaseID
                changes {
                    property
                    former_value
                }
            }
        }
    }
    """
        )
        params = {"id": str(id)}
        try:
            result = await client.execute(query, variable_values=params)
            return result["Award"][0]
        except IndexError or TransportProtocolError or TransportServerError:
            raise HTTPException(status_code=404, detail="Award not found")


# award based on supplier id
@router.get("/entities/awards/supplier/{id}", tags=["awards"])
async def read_award_per_org(id: uuid.UUID):
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]
    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})

    # TODO: extended_timetout could be lower, to be changed before releasing to production
    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:
        query = gql(
            """
    query getAward($id: String!) {
        Award(filter: {suppliers: {someHave: {id: {eq: $id}}}}) {
            id
            title
            description
            status
            date
            value {
                amount
                currency
            }
            suppliers {
                id
                name
                identifier {
                    scheme
                    legalName
                    uri
                }
                additionalIdentifiers {
                    scheme
                    legalName
                    uri
                }
                address {
                    countryCode
                    locality
                    postalCode
                    region
                    streetAddress 
                }
                contactPoint {
                    name
                    email
                    telephone
                    faxNumber
                    url
                }
                roles
                details
            }
            items {
                id
                description
                classification {
                    scheme
                    description
                }
                additionalClassifications {
                    scheme
                    description
                    uri
                }
                quantity
                unit {
                    scheme
                    name
                    value {
                        amount
                        currency
                    }
                    uri
                }
            }
            contractPeriod {
                startDate
                endDate
                maxExtentDate
                durationInDays
            }
            documents {
                documentType
                title
                description
                url
                datePublished
                dateModified
                format
                language
            }
            amendments {
                date
                rationale
                description
                amendsReleaseID
                releaseID
                changes {
                    property
                    former_value
                }
            }
            amendment {
               date
                rationale
                description
                amendsReleaseID
                releaseID
                changes {
                    property
                    former_value
                }
            }
        }
    }
    """
        )
        params = {"id": str(id)}
        try:
            result = await client.execute(query, variable_values=params)
            return result["Award"]
        except IndexError or TransportProtocolError or TransportServerError:
            raise HTTPException(status_code=404, detail="Award not found")
