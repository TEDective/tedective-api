# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later
import os
import uuid
import re

from fastapi import APIRouter, Depends, FastAPI, HTTPException, Query
from gql import Client, gql
from gql.transport.aiohttp import AIOHTTPTransport
from gql.transport.exceptions import TransportProtocolError, TransportServerError

router = APIRouter()


@router.get(
    "/entities/organization/{id}",
    tags=["organization"],
    description="Read organization by ID",
)
async def read_organization(id: uuid.UUID):
    """Return organization by ID

    :param id:
    """
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]

    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})
    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:
        query = gql(
            """
            query getOrganization($id: String!) {
                Organization (filter: {id: {eq: $id}}) {
                    id
                    name
                        identifier {
                            scheme
                            legalName
                            uri
                        }
                        additionalIdentifiers {
                            scheme
                            legalName
                            uri
                        }
                        address {
                            countryCode
                            locality
                            postalCode
                            region
                            streetAddress
                        }
                        contactPoint {
                            name
                            email
                            telephone
                            faxNumber
                            url
                        }
                        roles
                        details 
                    }
                }
            """
        )
        params = {"id": str(id)}
        try:
            result = await client.execute(query, variable_values=params)
            return result["Organization"][0]
        except IndexError or TransportProtocolError or TransportServerError:
            raise HTTPException(status_code=404, detail="Organization not found")


@router.get(
    "/entities/organization/search/{name}",
    tags=["organization"],
    description="Read organization by name",
)
async def search_organization(name: str):
    """Return organization by name 

    :param name:
    """
    address = os.environ["ADDRESS_GQL"]
    headers = os.environ["HEADERS"]

    transport = AIOHTTPTransport(url=address, headers={"Authorization": str(headers)})
    async with Client(
        transport=transport, fetch_schema_from_transport=True, execute_timeout=1000000
    ) as client:
        query = gql(
            """
            query getOrganizationSearch($name: String!) {
                Organization (filter: {name: {regex: $name}}) {
                    id
                    name
                }
            }
            """
        )
        params = {"name": "(?i)" +name}
        try:
            result = await client.execute(query, variable_values=params)
            unique_organizations = []   
            unique_ids = set()
            for org in result["Organization"]:
                if org["id"] not in unique_ids:
                    unique_organizations.append(org)
                    unique_ids.add(org["id"])
            return unique_organizations
        except IndexError or TransportProtocolError or TransportServerError:
            raise HTTPException(status_code=404, detail="Organization not found")
