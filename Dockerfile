# SPDX-FileCopyrightText: 2023 Free Software Foundation Europe <contact@fsfe.org>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# =============================================================================
# Create requirements.txt file
# =============================================================================
FROM python:3.11 AS requirements-builder

WORKDIR /root
ENV PATH="$PATH:/root/.local/bin"

# Upgrade / install pipx
RUN python -m pip install --user pipx
RUN python -m pipx ensurepath

# Install poetry with pipx
RUN python -m pipx install poetry

# Import Python packages
COPY pyproject.toml poetry.lock ./

# Export poetry env as requirements file
RUN poetry export -f requirements.txt --without-hashes > requirements.txt

# =============================================================================
# Create application
# =============================================================================
# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.11

EXPOSE 9000

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install required packages
RUN apt-get update && apt-get install --no-install-recommends --yes \
    libxml2-dev \
    libxslt-dev \
    cargo

# Install python requirements
COPY --from=requirements-builder /root/requirements.txt ./
RUN python -m pip install -r requirements.txt

# Copy code
WORKDIR /app
COPY tedective_api /app/tedective_api
COPY setup.py /app/setup.py
COPY tests /app/tests
COPY pytest.ini /app/pytest.ini

# Install TEDective API as a module
RUN python setup.py install

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
RUN adduser -u 5678 --disabled-password --gecos "" appuser && chown -R appuser /app
USER appuser

# During debugging, this entry point will be overridden. For more information, please refer to https://aka.ms/vscode-docker-python-debug
CMD ["gunicorn", "--bind", "0.0.0.0:9000", "-k", "uvicorn.workers.UvicornWorker", "tedective_api.main:app"]
